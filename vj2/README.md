# Vježba 2 - Sustav čestica

![Tisuće pahulja na tamnoplavoj pozadini](./doc/pahulje.png)

Program simulira padajući snijeg sustavom čestica.

## Build

Potreban je CMake verzije najmanje 3.10 te C++ prevodioc koji podržava standard
C++17. Također su potrebni CMake paketi OpenGL, GLEW, SDL2, glm i DevIL.

Navedeni koraci su za Linux, na Windowsima je postupak vjerojatno malo drugačiji.

1. Otvorite novi direktorij (npr `build`)
2. U direktoriju `build`  pokrenite naredbu `cmake ..`
3. Trebale bi se generirati build datoteke za neki alat (tipično `make`).
4. Pokrenite naredbu `make`.
5. Trebala bi se izgraditi izvršna datoteka `src/particles`.

## Pokretanje

Program ignorira argumente komandne linije.

Program simulira padajući snijeg sustavom čestica. Svake sekunde se iz zraka
stvara oko 2000 čestica koje se crtaju kao pahulje snijega. Svaka pahulja započinje
kretanjem u slučajno odabranom smjeru i ima slučajno odabrani koeficijent otpora
zraka. Kada čestice padnu na tlo, polako nestaju.

Pritiskom strelica kamera se rotira oko scene. Pritiskom na `wasd` tipke moguće
je dodati utjecaj vjetra na simulaciju: `w` i `s` pojačavaju vjetar u smjeru
naprijed-nazad, a `a` i `d` pojačavaju vjetar u smjeru lijevo-desno, u odnosu na
smjer pogleda.

Uz pozicije čestica još se prenose i vektori brzine čestica. Pritiskom na
`t` boja pahuljica se mijenja ovisno o njezinom vektoru kretanja.

## Komentari

Za usmjeravanje kamere se koristi up-vektor. No kameru je moguće okrenuti naopako
(više od 90 stupnjeva prema gore/dolje), i kada se to dogodi kontrole kamere su
obrnute no pogled je još uvijek uspravan. Bilo bi bolje ili ograničiti kretanje
kamere, ili iskoristiti kvaternion kojim se određuje pozicija kamere na sferi za
orijentaciju kamere, tako da kontrole ostaju jasne čak i kada je kamera naopako.

Izračun pozicija čestica može se paralelizirati ili izvesti na GPU, radi bržeg
izvođenja. Kod izvođenja na GPU bi se smanjila količina podataka koja se treba
prenijeti između sistemske i grafičke memorije, no dodatni račun bi mogao
usporiti crtanje, pa se taj pristup treba pažljivo ispitati.

Za crtanje pahulja se koriste point-spriteovi OpenGL-a koji imaju svoje
probleme: clipaju se kao točke pa nestaju s rubova ekrana prije nego što u
potpunosti izađu s njega, i ne mogu se rotirati (no može se rotirati tekstura).
Umjesto toga svaka pahulja je mogla biti quad koji bi poravnali prema kameri
(billboarding).

Kada pahulja padne na tlo, više se ne pomiče. No njezin vektor brzine ostaje
nepromijenjen, što se može vidjeti kada promijenimo boju pahuljica s `t`.
