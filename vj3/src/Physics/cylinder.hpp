#pragma once

#include <glm/vec3.hpp>

namespace Physics {

// cylinder off y axis
struct cylinder {
  float halfheight;
  float radius;
};

}