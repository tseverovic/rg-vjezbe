/**
 * @file Streams.hpp
 * @author Tomislav Severović
 * @brief Various streams.
 * @version 0.1
 * @date 2019-01-14
 * 
 * @copyright Copyright (c) 2019 Tomislav Severović
 * 
 */
#pragma once

#include <ostream>

namespace Streams {

namespace detail {
/**
 * @brief std::streambuf which does nothing.
 */
struct dummy_streambuf : public std::streambuf {
  dummy_streambuf() noexcept : std::streambuf{} {}
};

}  // namespace detail

inline detail::dummy_streambuf dummy_streambuf;

/**
 * @brief std::ostream which does nothing.
 */
struct dummy_ostream : public std::ostream {
  dummy_ostream() noexcept : std::ostream{&dummy_streambuf} {}
  dummy_ostream(dummy_ostream&&) noexcept  // pretend we move it!
      : dummy_ostream{} {}
};

}  // namespace Streams