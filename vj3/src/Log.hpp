/**
 * @file Log.hpp
 * @author Tomislav Severović
 * @brief Simple logging framework.
 * @version 0.1
 * @date 2019-01-14
 * 
 * Simple logging framework with compile-time verbosity setting.
 * 
 * @copyright Copyright (c) 2019 Tomislav Severović
 * 
 */
#pragma once

#include <ostream>
#include <type_traits>
#include <sstream>

#include "Streams.hpp"
#include "Util.hpp"

namespace Log {

/**
 * @brief Verbosity settings.
 */
enum class verbosity : int {
  silent = 0,  ///< Nothing here!
  error = 1,   ///< Errors happening.
  warn = 2,    ///< Warnings for potential errors.
  info = 3,    ///< Various informative messages.
  debug = 4,   ///< Debug information.
  spew = 5,    ///< Just spews info. Slow.
};

/**
 * @brief Initializes the log structures.
 * 
 * @param argv0 Name of the program.
 * @param file  Filename to write log to. Can also be stdout or stderr.
 */
auto init(std::string_view argv0, std::string_view file) noexcept -> void;

/**
 * @brief A view to the log stream.
 * 
 * Supports the std::ostream interface.
 */
class logstream : public std::ostream {
  // TODO: intercept flush.

  using base_stream = std::ostream;

  /**
   * @brief Buffer for stream.
   * Could be optimized out later, to minimize allocations.
   */
  std::stringbuf buf_;

 public:
  /**
   * @brief Construct a new logstream.
   */
  logstream() noexcept;

  /**
   * @brief Flush stream to the log file.
   */
  virtual ~logstream() noexcept override;

  logstream(logstream&&) noexcept;

  template <verbosity V>
  friend struct logger;
  friend auto init(std::string_view argv0, std::string_view file) noexcept -> void;
};

/**
 * @brief Template class to access log stream.
 * 
 * Recommended use:
 * using log = Log::logger<Log::verbosity::info>;
 * log::info() << "etc.";
 * 
 * @tparam V verbosity.
 */
template <verbosity V>
struct logger {
 private:
  template <verbosity V1>
  using stream_type =
      std::conditional_t<V >= V1, logstream, Streams::dummy_ostream>;

 public:
  static constexpr auto verbosity = V;
  static auto spew() noexcept {
    stream_type<verbosity::spew> stream{};
    stream << "Spew: ";
    return stream;
  }
  static auto debug() noexcept {
    stream_type<verbosity::debug> stream{};
    stream << "Debug: ";
    return stream;
  }
  static auto info() noexcept {
    stream_type<verbosity::info> stream{};
    stream << "Info: ";
    return stream;
  }
  static auto warn() noexcept {
    stream_type<verbosity::warn> stream{};
    stream << "Warning: ";
    return stream;
  }
  static auto error() noexcept {
    stream_type<verbosity::error> stream{};
    stream << "Error: ";
    return stream;
  }
};

}  // namespace Log