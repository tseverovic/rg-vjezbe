#pragma once

#include <vector>
#include <utility>

#include <GL/glew.h>
#include <glm/vec3.hpp>

namespace Meshes {

struct vert_data {
  glm::vec3 pos;
  glm::vec3 normal;
};

using mesh_arrays = std::pair<std::vector<vert_data>, std::vector<GLuint>>;

auto box_mesh() -> mesh_arrays;
auto sphere_mesh(unsigned lat_zones, unsigned long_zones) -> mesh_arrays;

struct object_draw_data {
  GLuint vao;
  GLuint verts;
};

auto send_mesh(std::vector<GLuint>& vao, std::vector<GLuint>& buffers,
               mesh_arrays const& arrays) -> object_draw_data;

}  // namespace Meshes