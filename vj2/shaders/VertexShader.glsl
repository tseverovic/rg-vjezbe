#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 speed;
layout(location = 2) in float fade;

out vec3 vertex_color;

uniform mat4 MV;
uniform mat4 P;
uniform bool use_speed_for_color;
uniform float near_height;

const float terminal_velocity_1 = 0.3f / 0.981f;
const float zNear = 1.0f;
const float zFar = 20.0f;

void main() {
  vec4 v_pos = MV * vec4(position, 1.0f);
  gl_Position = P * v_pos;

  // [1, 20] distance from camera is scaled into [1, 0] white to black
  vertex_color = vec3(clamp((-length(v_pos.xyz) + zNear) / (zFar - zNear) + zNear, 0.0f, 1.0f));

  if (use_speed_for_color) {
    // TODO: find if there's a prettier way of doing this.
    vertex_color.r *= min(1.0f, abs(speed.r * terminal_velocity_1));
    vertex_color.g *= min(1.0f, abs(speed.g * terminal_velocity_1));
    vertex_color.b *= min(1.0f, abs(speed.b * terminal_velocity_1));
  }
  vertex_color *= fade;
  gl_PointSize = near_height * 0.1f / gl_Position.w;
}
