#include "Collision.hpp"

#include <algorithm>
#include <limits>

namespace Physics {

bool collides(sphere const& s1, phys_data const& d1,
              sphere const& s2, phys_data const& d2) noexcept {
  auto dist = d2.pos - d1.pos;
  auto radii = s1.radius + s2.radius;
  return glm::dot(dist, dist) < radii * radii;
}

auto closest_point_obb(glm::vec3 p, Physics::box const& b, glm::vec3 b_pos,
                       glm::quat b_rot) noexcept {
  auto c_dist = p - b_pos;
  auto result = b_pos;

  glm::mat3 axes = glm::mat3_cast(b_rot);
  for (int i = 0; i < 3; ++i) {
    float extent = i == 0 ? b.halfdepth : i == 1 ? b.halfheight : b.halfdepth;
    float dist = std::clamp(glm::dot(c_dist, axes[i]), -extent, extent);

    result += dist * axes[i];
  }

  return result;
}

bool collides(Physics::sphere const& s, phys_data const& d1,
              Physics::box const& b, phys_data const& d2) noexcept {
  auto v = closest_point_obb(d1.pos, b, d2.pos, d2.rot) - d1.pos;
  return glm::dot(v, v) < s.radius * s.radius;
}

bool collides(Physics::box const& b, phys_data const& d1,
              Physics::sphere const& s, phys_data const& d2) noexcept {
  return collides(s, d2, b, d1);
}

bool collides(Physics::box const& b1, phys_data const& d1,
              Physics::box const& b2, phys_data const& d2) noexcept {
  using namespace glm;
  float ra, rb;
  // cast is transform matrix from local to global coordinate space
  // (axes in columns)
  mat3 b1_axes = mat3_cast(d1.rot);
  mat3 b2_axes = mat3_cast(d2.rot);

  mat3 R;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      R[i][j] = dot(b1_axes[i], b2_axes[j]);
    }
  }

  auto t = d2.pos - d1.pos;
  t = vec3{dot(t, b1_axes[0]), dot(t, b1_axes[1]), dot(t, b1_axes[2])};

  mat3 AbsR;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      AbsR[i][j] = abs(R[i][j]) + std::numeric_limits<float>::epsilon();
    }
  }

  // axes L = A0, A1, A2
  for (int i = 0; i < 3; ++i) {
    ra = i == 0 ? b1.halfwidth : i == 1 ? b1.halfheight : b1.halfdepth;
    rb = dot(vec3{b2.halfwidth, b2.halfheight, b2.halfdepth}, AbsR[i]);
    if (abs(t[i]) > ra + rb) return false;
  }

  // axes L = B0, B1, B2
  for (int i = 0; i < 3; ++i) {
    ra = dot(vec3{b1.halfwidth, b1.halfheight, b1.halfdepth},
             transpose(AbsR)[i]);
    rb = i == 0 ? b2.halfwidth : i == 1 ? b2.halfheight : b2.halfdepth;
    if (abs(dot(t, transpose(R)[i])) > ra + rb) return false;
  }

  // L = A0 X B0
  ra = b1.halfheight * AbsR[2][0] + b1.halfdepth * AbsR[1][0];
  rb = b2.halfheight * AbsR[0][2] + b2.halfdepth * AbsR[0][1];
  if (abs(t[2] * R[1][0] - t[1] * R[2][0]) > ra + rb) return false;

  // L = A0 X B1
  ra = b1.halfheight * AbsR[2][1] + b1.halfdepth * AbsR[1][1];
  rb = b2.halfwidth * AbsR[0][2] + b2.halfdepth * AbsR[0][0];
  if (abs(t[2] * R[1][2] - t[1] * R[2][2]) > ra + rb) return false;

  // L = A0 X B2
  ra = b1.halfheight * AbsR[2][2] + b1.halfdepth * AbsR[1][2];
  rb = b2.halfwidth * AbsR[0][1] + b2.halfheight * AbsR[0][0];
  if (abs(t[2] * R[1][2] - t[1] * R[2][2]) > ra + rb) return false;

  // L = A1 X B0
  ra = b1.halfwidth * AbsR[2][0] + b1.halfdepth * AbsR[0][0];
  rb = b2.halfheight * AbsR[1][2] + b2.halfdepth * AbsR[1][1];
  if (abs(t[0] * R[2][0] - t[2] * R[0][0]) > ra + rb) return false;

  // L = A1 X B1
  ra = b1.halfwidth * AbsR[2][1] + b1.halfdepth * AbsR[0][1];
  rb = b2.halfwidth * AbsR[1][2] + b2.halfdepth * AbsR[1][0];
  if (abs(t[0] * R[2][1] - t[2] * R[0][1]) > ra + rb) return false;

  // L = A1 X B2
  ra = b1.halfwidth * AbsR[2][2] + b1.halfdepth * AbsR[0][1];
  rb = b2.halfwidth * AbsR[1][1] + b2.halfheight * AbsR[1][0];
  if (abs(t[0] * R[2][2] - t[2] * R[0][2]) > ra + rb) return false;

  // L = A2 X B0
  ra = b1.halfwidth * AbsR[1][0] + b1.halfheight * AbsR[0][0];
  rb = b2.halfheight * AbsR[2][2] + b2.halfheight * AbsR[2][1];
  if (abs(t[1] * R[0][0] - t[0] * R[1][0]) > ra + rb) return false;

  // L = A2 X B1
  ra = b1.halfwidth * AbsR[1][1] + b1.halfheight * AbsR[0][1];
  rb = b2.halfwidth * AbsR[2][2] + b2.halfdepth * AbsR[2][0];
  if (abs(t[1] * R[0][1] - t[0] * R[1][1]) > ra + rb) return false;

  // L = A2 X B2
  ra = b1.halfwidth * AbsR[1][2] + b1.halfheight * AbsR[0][2];
  rb = b2.halfwidth * AbsR[2][1] + b2.halfheight * AbsR[2][0];
  if (abs(t[1] * R[0][2] - t[0] * R[1][2]) > ra + rb) return false;

  return true;
}
}