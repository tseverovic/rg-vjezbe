#version 440 core 

in vec3 v_pos;
in vec3 v_normal;

out vec3 out_color;

layout(std140, binding = 0)
uniform LightData {
  vec3 pos;
  vec3 color;
} light;

layout(std140, binding = 1)
uniform Material {
  vec3 diffuse_color;
  vec3 spec_color;
  vec3 ambient_color;
  float spec;
} material;

float half_lambert(float lambert) {
  float hl = lambert * 0.5f + 0.5f;
  return hl * hl;
}

void main() {
  vec3 normal = normalize(v_normal);
  vec3 light_dist = light.pos - v_pos;

  // light falloff
  float falloff = 1 + 0.001 * dot(light_dist, light_dist);

  vec3 light_dir = normalize(light_dist);
  vec3 view_dir = normalize(-v_pos);

  float lambert = dot(light_dir, normal);
  
  float specular = max(0.0f, lambert)
                 * pow(dot(normalize(light_dir + view_dir), // H-vector
                           normal), material.spec);

  out_color = light.color * (half_lambert(lambert) * material.diffuse_color
                             + specular * material.spec_color) / falloff 
            + material.ambient_color;
}