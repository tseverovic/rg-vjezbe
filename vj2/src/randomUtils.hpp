#include <random>

#include <glm/glm.hpp>

namespace RandUtil {

thread_local std::default_random_engine rng{std::random_device{}()};

template <typename T>
auto uniformRandom(T a, T b) {
  if constexpr (std::is_floating_point_v<T>) {
    return std::uniform_real_distribution<T>{a, b}(rng);
  } else {
    return std::uniform_int_distribution<T>{a, b}(rng);
  }
}

template <typename T>
auto exponentialRandom(T x) {
  return std::exponential_distribution{x}(rng);
}

auto randomCone(glm::vec3 dir, float spread) {
  dir = glm::normalize(dir);
  glm::vec3 diff = (std::abs(dir.x) < 0.5f) ? glm::vec3{1.0f, 0.0f, 0.0f}
                                            : glm::vec3{0.0f, 1.0f, 0.0f};
  glm::vec3 b1 = glm::normalize(glm::cross(dir, diff));
  glm::vec3 b2 = glm::cross(b1, dir);

  float z = uniformRandom<float>(std::cos(spread * M_PI), 1.0f);
  float r = std::sqrt(1.0f - z * z);
  float theta = uniformRandom<float>(-M_PI, M_PI);
  float x = r * std::cos(theta);
  float y = r * std::sin(theta);
  return x * b1 + y * b2 + z * dir;
}
}