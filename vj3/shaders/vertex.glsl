#version 440 core 

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;

out vec3 v_pos;
out vec3 v_normal;

layout(location = 0) uniform mat4 MV;
layout(location = 1) uniform mat4 MV_inv_transpose;
layout(location = 2) uniform mat4 P;

void main() {
  vec4 v_pos_4 = MV * vec4(pos, 1.0);
  gl_Position = P * v_pos_4;

  v_pos = vec3(v_pos_4);
  v_normal = vec3(MV_inv_transpose * vec4(normal, 1.0));
}