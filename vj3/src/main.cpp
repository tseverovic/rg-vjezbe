#include <iostream>
#include <string_view>

#include <GL/glew.h>

#include <SDL2/SDL.h>

#include "Log.hpp"

#include "Game/Main.hpp"

using logger = Log::logger<Log::verbosity::debug>;
using namespace std::literals;

constexpr auto gl_debug_source_string(GLenum source) noexcept {
  switch (source) {
    case GL_DEBUG_SOURCE_API:
      return "base"sv;
    case GL_DEBUG_SOURCE_APPLICATION:
      return "app"sv;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
      return "win"sv;
    case GL_DEBUG_SOURCE_THIRD_PARTY:
      return "3rdp"sv;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
      return "shaders"sv;
    case GL_DEBUG_SOURCE_OTHER:
    default:
      return ""sv;
  }
}

constexpr auto gl_debug_type_string(GLenum type) noexcept {
  switch (type) {
    case GL_DEBUG_TYPE_ERROR:
      return "ERR"sv;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      return "OLD"sv;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      return "UB"sv;
    case GL_DEBUG_TYPE_PORTABILITY:
      return "PORT"sv;
    case GL_DEBUG_TYPE_PERFORMANCE:
      return "PERF"sv;
    case GL_DEBUG_TYPE_MARKER:
      return "MARK"sv;
    case GL_DEBUG_TYPE_PUSH_GROUP:
      return "{"sv;
    case GL_DEBUG_TYPE_POP_GROUP:
      return "}"sv;
    case GL_DEBUG_TYPE_OTHER:
    default:
      return ""sv;
  }
}

using gl_logger = Log::logger<Log::verbosity::debug>;
extern "C" void GLAPIENTRY gl_debug_callback(GLenum source, GLenum type,
                                             GLuint id, GLenum severity,
                                             GLsizei length,
                                             const GLchar* message,
                                             const void*) noexcept {
  constexpr auto gl_debug_prefix = "GL: "sv;
  auto source_str = gl_debug_source_string(source);
  auto type_str = gl_debug_type_string(type);

  switch (severity) {
    case GL_DEBUG_SEVERITY_NOTIFICATION:
      gl_logger::spew() << gl_debug_prefix << source_str << ' ' << type_str
                        << '(' << id << "): " << message;
      break;
    case GL_DEBUG_SEVERITY_LOW:
      gl_logger::debug() << gl_debug_prefix << source_str << ' ' << type_str
                         << '(' << id << "): " << message;
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      gl_logger::warn() << gl_debug_prefix << source_str << ' ' << type_str
                        << '(' << id << "): " << message;
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      gl_logger::error() << gl_debug_prefix << source_str << ' ' << type_str
                         << '(' << id << "): " << message;
      break;
    default:
      break;
  }
}

auto init_GL() -> SDL_Window* {
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

#ifndef NDEBUG
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif

  SDL_Window* window =
      SDL_CreateWindow("Collision Demo", SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_OPENGL);
  if (!window) {
    logger::error() << "Failed to open window: " << SDL_GetError();
    std::exit(-1);
  }
  logger::spew() << "Opened window";

  if (!SDL_GL_CreateContext(window)) {
    logger::error() << "Failed to create context: " << SDL_GetError();
    std::exit(-1);
  }
  logger::spew() << "Created context";

  if (GLenum err = glewInit(); err != GLEW_NO_ERROR) {
    logger::error() << "Failed to initialize GLEW: " << glewGetErrorString(err);
    std::exit(-1);
  }
  logger::spew() << "Loaded GL functions";

  if (gl_logger::verbosity != Log::verbosity::silent) {
    glEnable(GL_DEBUG_OUTPUT);
    glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(gl_debug_callback, nullptr);
  }
  if (gl_logger::verbosity < Log::verbosity::spew) {
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
                          GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, GL_FALSE);
  }
  if (gl_logger::verbosity < Log::verbosity::debug) {
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_LOW, 0,
                          nullptr, GL_FALSE);
  }
  if (gl_logger::verbosity < Log::verbosity::info) {
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM,
                          0, nullptr, GL_FALSE);
  }

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);

  return window;
}

auto main(int argc, char** argv) -> int {
  Log::init(argv[0], "stderr");
  logger::spew() << "Initialized logging";

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_TIMER)) {
    logger::error() << "Failed to initialize SDL: " << SDL_GetError();
    std::exit(-1);
  }
  std::atexit(SDL_Quit);
  logger::info() << "Sucessfully initialized SDL";

  SDL_Window* window = init_GL();

  Game::main(window);

  SDL_DestroyWindow(window);
  SDL_GL_DeleteContext(SDL_GL_GetCurrentContext());

  return 0;
}