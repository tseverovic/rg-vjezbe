#include <chrono>
#include <vector>
#include <algorithm>
#include <random>
#include <iostream>
#include <utility>

#include <GL/glew.h>
#include <SDL2/SDL.h>

#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>

#include "randomUtils.hpp"
#include "shaderLoader.hpp"

using namespace std::literals;

template <typename T1, typename T2, typename T3>
struct triple {
  T1 first;
  T2 second;
  T3 third;
};

using Particle = triple<glm::vec3, glm::vec3, float>;

SDL_Window* window = nullptr;

float camera_dist = 5.0f;
glm::vec3 camera_rot{0.0f, 0.0f, 0.0f};
float camera_fov = 45.0f;
glm::vec3 camera_look_at{0.0f, 2.5f, 0.0f};

glm::vec3 wind_dir{0.0f, 0.0f, 0.0f};

int window_x = 640;
int window_y = 480;

bool resized = true;

bool use_speed_for_color = false;

auto resize(int x, int y) {
  window_x = x;
  window_y = y;
  resized = true;
}

auto checkError() {
  if (GLenum err = glGetError()) {
    std::cerr << "Error in GL: " << gluErrorString(err) << '\n';
  } else {
    std::cerr << "Clear.\n";
  }
}

auto resolutionRefresh() {
  if (!resized) return;

  glViewport(0, 0, window_x, window_y);

  resized = false;
}

constexpr float low_drag_coef = 0.3f;
constexpr float high_drag_coef = 0.9f;
constexpr float gravity_const = 0.981f;
constexpr glm::vec3 gravity{0.0f, -gravity_const, 0.0f};
constexpr float terminal_velocity = gravity_const / low_drag_coef;
constexpr float particle_fade_sec = 0.05f;

std::vector<Particle> particles_phys;
std::vector<float> drag_coefs;

auto doPhysicsEuler(Particle& particle, float drag_coef,
                    std::chrono::duration<float> elapsed) {
  auto& [pos, spd, fade] = particle;
  spd += elapsed.count() * (-(spd - wind_dir) * drag_coef + gravity);
  pos += elapsed.count() * spd;
}

int max_flakes = 200000;
constexpr float spawn_rate_per_sec = 2000;

auto traverseParticles(std::chrono::duration<float> elapsed) {
  int i;
  int gap = 0;
  for (i = 0; i < particles_phys.size(); i++) {
    Particle& p = particles_phys[i];
    if (p.first.y > 0.0f) {
      doPhysicsEuler(particles_phys[i], drag_coefs[i], elapsed);
    } else if (p.third > 0.0f) {
      p.third -= elapsed.count() * particle_fade_sec;
    } else {
      gap = 1;
      break;
    }
  }
  if (gap == 0) return;

  while (i + gap < particles_phys.size()) {
    std::swap(particles_phys[i], particles_phys[i + gap]);
    std::swap(drag_coefs[i], drag_coefs[i + gap]);
    Particle& p = particles_phys[i];
    if (p.first.y > 0.0f) {
      doPhysicsEuler(p, drag_coefs[i], elapsed);
      i++;
    } else if (p.third > 0.0f) {
      p.third -= elapsed.count() * particle_fade_sec;
      i++;
    } else {
      gap++;
    }
  }
  particles_phys.erase(particles_phys.end() - gap, particles_phys.end());
  drag_coefs.erase(drag_coefs.end() - gap, drag_coefs.end());
  return;
}

auto animate(std::chrono::duration<float> elapsed) {
  traverseParticles(elapsed);

  while (particles_phys.size() < max_flakes && elapsed > 0s) {
    particles_phys.push_back({
        glm::vec3{RandUtil::uniformRandom(-20.0f, 20.0f), 10.0f,
                  RandUtil::uniformRandom(-20.0f, 20.0f)},
        glm::normalize(RandUtil::randomCone(gravity, glm::radians(20.0)) +
                       wind_dir) *
            terminal_velocity,
        1.0f});
    drag_coefs.push_back(
        RandUtil::uniformRandom(low_drag_coef, high_drag_coef));
    elapsed -= std::chrono::duration<float>{
        RandUtil::exponentialRandom(spawn_rate_per_sec)};
  }
}

GLuint vao;
GLuint vbo;
GLuint shader;
GLuint tex;

auto setUniformImpl(GLuint location, GLint value) {
  return glUniform1i(location, value);
}

auto setUniformImpl(GLuint location, GLuint value) {
  return glUniform1ui(location, value);
}

auto setUniformImpl(GLuint location, GLfloat value) {
  return glUniform1f(location, value);
}

auto setUniformImpl(GLuint location, bool value) {
  return glUniform1i(location, value);
}

auto setUniformImpl(GLuint location, const glm::mat4& value) {
  return glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
}

template <typename T>
auto setUniform(GLuint shader, std::string_view name, T&& value) {
  return setUniformImpl(glGetUniformLocation(shader, name.data()), std::forward<T>(value));
}

auto setTexture(GLuint shader, std::string_view name, GLuint value) {
  return glUniform1i(glGetUniformLocation(shader, name.data()), value);
}

auto drawParticles(const glm::mat4& P, const glm::mat4& V) {
  glBindVertexArray(vao);

  glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, max_flakes * sizeof(Particle), NULL,
               GL_STREAM_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, particles_phys.size() * sizeof(Particle),
                  particles_phys.data());

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle),
                        (void*)offsetof(Particle, second));
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle),
                        (void*)offsetof(Particle, third));

  setUniform(shader, "P", P);
  setUniform(shader, "MV", V);
  setUniform(shader, "use_speed_for_color", use_speed_for_color);
  setTexture(shader, "tex", 0);
  setUniform<GLfloat>(shader, "near_height", window_y / (2.0 * std::tan(0.5 * glm::radians(camera_fov))));

  glDrawArrays(GL_POINTS, 0, particles_phys.size());
  glBindVertexArray(0);
}

auto draw() {
  resolutionRefresh();

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::vec3 camera_pos =
      glm::rotate(glm::quat{camera_rot}, glm::vec3{0.0f, 0.0f, 1.0f}) *
          camera_dist +
      camera_look_at;

  glm::mat4 lookAtMat =
      glm::lookAt(camera_pos, camera_look_at, glm::vec3{0.0f, 1.0f, 0.0});

  glm::mat4 projectionMat = glm::perspective(
      glm::radians(camera_fov), (float)window_x / window_y, 1.0f, 100.0f);

  drawParticles(projectionMat, lookAtMat);
}

auto loadImage(std::string_view fname) -> std::optional<ILuint> {
  ILuint image;
  ilGenImages(1, &image);
  ilBindImage(image);
  if (ilLoadImage(fname.data())) {
    if (ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE)) {
      return image;
    }
  }
  ilBindImage(0);
  ilDeleteImage(image);
  return std::nullopt;
}

auto initSystem() {
  for (int i = 0; i < 1000; i++) {
    float height = RandUtil::uniformRandom(0.0f, 10.0f);
    particles_phys.push_back({
        glm::vec3{RandUtil::uniformRandom(-20.0f, 20.0f), height,
                  RandUtil::uniformRandom(-20.0f, 20.0f)},
        glm::normalize(RandUtil::randomCone(gravity, glm::radians(20.0)) +
                       wind_dir * (height * 0.1f)) *
            terminal_velocity,
        1.0f});
    drag_coefs.push_back(
        RandUtil::uniformRandom(low_drag_coef, high_drag_coef));
  }

  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Particle) * max_flakes, NULL,
               GL_STREAM_DRAW);
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  if (auto image_o = loadImage("res/snow.bmp")) {
    auto& image = *image_o;

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ilGetInteger(IL_IMAGE_WIDTH), 
      ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData());
    ilBindImage(0);
    ilDeleteImage(image);
  } else {
    std::cerr << "Failed to open image.\n";
    std::exit(-1);
  }
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glGenerateMipmap(GL_TEXTURE_2D);

  if (auto shaderProgram_o =
          loadShader("shaders/VertexShader.glsl", "shaders/PixelShader.glsl")) {
    shader = *shaderProgram_o;
    glUseProgram(shader);
  } else {
    std::cerr << "Failed to create shaders.\n";
    std::exit(-1);
  }

  glClearColor(0.0f, 0.08f, 0.2f, 1.0f);
}

auto main(int argc, char** argv) -> int {
  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
  ilInit();
  iluInit();

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

  window = SDL_CreateWindow("Snijeg", SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, window_x, window_y,
                            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  SDL_GLContext context = SDL_GL_CreateContext(window);

  GLenum err = glewInit();
  if (err != GLEW_NO_ERROR) {
    std::cerr << "Failed to load GLEW: " << glewGetErrorString(err) << '\n';
    return -1;
  }

  glEnable(GL_PROGRAM_POINT_SIZE);
  glEnable(GL_BLEND);
  SDL_GL_SetSwapInterval(1);

  initSystem();

  bool running = true;
  auto running_since = std::chrono::system_clock::now();
  auto last_draw = running_since - 33ms;


  const Uint8* keystates = SDL_GetKeyboardState(nullptr);
  while (running) {
    // auto now = std::chrono::system_clock::now();
    // animate(now - last_draw);
    // last_draw = now;
    animate(33ms);
    draw();
    SDL_GL_SwapWindow(window);

    SDL_Event ev;
    while (SDL_PollEvent(&ev)) {
      switch (ev.type) {
        case SDL_KEYUP: {
          SDL_KeyboardEvent ke = ev.key;
          switch (ke.keysym.sym) {
            case SDLK_ESCAPE:
              running = false;
              break;
          }
        } break;
        case SDL_KEYDOWN: {
          SDL_KeyboardEvent ke = ev.key;
          switch (ke.keysym.sym) {
            case SDLK_t:
              use_speed_for_color = !use_speed_for_color;
              break;
          }
        }
        case SDL_WINDOWEVENT: {
          SDL_WindowEvent we = ev.window;
          switch (we.event) {
            case SDL_WINDOWEVENT_CLOSE:
              running = false;
              break;
            case SDL_WINDOWEVENT_RESIZED:
              resize(we.data1, we.data2);
              break;
          }
        }
      }
    }

    if (keystates[SDL_SCANCODE_LEFT]) {
      camera_rot.y -= 0.02;
    }
    if (keystates[SDL_SCANCODE_RIGHT]) {
      camera_rot.y += 0.02;
    }
    if (keystates[SDL_SCANCODE_UP]) {
      camera_rot.x -= 0.02;
    }
    if (keystates[SDL_SCANCODE_DOWN]) {
      camera_rot.x += 0.02;
    }

    glm::vec3 wind_forward_dir =
        glm::rotate(glm::quat{camera_rot}, glm::vec3{0.0f, 0.0f, -1.0f});
    wind_forward_dir.y = 0.0f;
    wind_forward_dir = glm::normalize(wind_forward_dir);
    glm::vec3 wind_left_dir =
        glm::cross(glm::vec3{0.0f, 1.0f, 0.0f}, wind_forward_dir);

    if (keystates[SDL_SCANCODE_W]) {
      wind_dir += wind_forward_dir * 0.02f;
    }
    if (keystates[SDL_SCANCODE_S]) {
      wind_dir -= wind_forward_dir * 0.02f;
    }
    if (keystates[SDL_SCANCODE_A]) {
      wind_dir += wind_left_dir * 0.02f;
    }
    if (keystates[SDL_SCANCODE_D]) {
      wind_dir -= wind_left_dir * 0.02f;
    }
  }

  SDL_DestroyWindow(window);
  SDL_GL_DeleteContext(context);
  SDL_Quit();
  return 0;
}