#pragma once

#include <string_view>
#include <optional>

#include <GL/glew.h>

auto loadShader(std::string_view vsh, std::string_view fsh) -> std::optional<GLuint>;
