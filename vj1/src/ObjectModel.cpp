#include "ObjectModel.hpp"

#include <istream>

/**
 * \brief Reads an \p ObjectModel from an <tt>std::istream</tt>.
 *
 * \param[out] is <tt>std::istream</tt> to read from.
 * \param[in]  m  \p ObjectModel to read in.
 * \return \p is
 */
auto operator>>(std::istream& is, ObjectModel& m) -> std::istream& {
  char c;
  int line = 1;
  while (is.get(c)) {
    switch (c) {
      case 'v': {
        float x, y, z;
        is >> x >> y >> z;
        m.verts_.push_back({{x, y, z}, {0.0, 0.0, 0.0}});
        is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        line++;
        break;
      }
      case 'f': {
        TriIndices face;
        for (int i = 0; i < 3; i++) {
          is >> face.is[i];
          face.is[i]--;
        }
        m.indices_.push_back(std::move(face));
        is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        line++;
        break;
      }
      case '#':
        is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        line++;
        break;
      case ' ':
        is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        line++;
        break;
      case '\n':
        line++;
        break;
      default:
        if (is.bad()) return is;
    }
  }

  m.faces_.resize(m.indices_.size());

  // coeffs
  m.compute_normals();
  return is;
}

void ObjectModel::compute_normals() {
  std::vector<int> face_count(verts_.size());

  for (std::size_t j = 0; j < indices_.size(); j++) {
    const TriIndices& i = indices_[j];
    auto& p0 = verts_[i.is[0]];
    auto& p1 = verts_[i.is[1]];
    auto& p2 = verts_[i.is[2]];
    face_count[i.is[0]]++;
    face_count[i.is[1]]++;
    face_count[i.is[2]]++;

    math::Vector<3> v1 = p1.vert - p0.vert;
    /* {p1.vert[0] - p0.vert[0],
                        p1.vert[1] - p0.vert[1],
                        p1.vert[2] - p0.vert[2]}; */
    math::Vector<3> v2 = p2.vert - p0.vert;
    /* {p2.vert[0] - p0.vert[0],
                        p2.vert[1] - p0.vert[1],
                        p2.vert[2] - p0.vert[2]}; */

    math::Vector<3> vn = v1.x(v2);
    vn.normalize();

    faces_[j] = Face3D{vn[0], vn[1], vn[2],
                       -(p0.vert * vn)};  // (-p0.vert[0] * vn[0] - p0.vert[1] *
                                          // vn[1] - p0.vert[2] * vn[2])};

    p0.normal += vn;
    p1.normal += vn;
    p2.normal += vn;
  }

  for (std::size_t i = 0; i < verts_.size(); i++) {
    auto& n = verts_[i].normal;
    int count = face_count[i];
    n /= (double)count;
  }
}

template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...)->overloaded<Ts...>;

void ObjectModel::draw() const {
  glPushMatrix();

  glPushMatrix();
  auto translate = math::translate3D(pos_[0], pos_[1], pos_[2]);
  glMultMatrixd(std::addressof(translate(0, 0)));

  // move light with object but keep it in place as object rotates
  constexpr float light_position[] = {5.0f, 5.0f, 3.0f, 1.0f};
  constexpr float light_ambient[] = {0.0f, 0.1f, 0.5f, 1.0f};
  constexpr float light_diffuse[] = {0.9f, 0.8f, 0.7f, 1.0f};
  constexpr float light_spec[] = {0.9f, 0.9f, 0.8f, 1.0f};
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);
  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light_spec);
  glPopMatrix();

  // materials
  float material_ambient[] = {0.8f, 0.8f, 0.8f, 1.0f};
  float material_diffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
  float material_spec[] = {0.1f, 0.1f, 0.1f, 1.0f};
  glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR, material_spec);
  glMaterialf(GL_FRONT, GL_SHININESS, 96.0f);

  glMultMatrixd(std::addressof(translate(0, 0)));
  std::visit(overloaded {
    [](const DCMRotate& dcm) {
      glMultMatrixd(std::addressof(dcm(0, 0)));
    },
    [](const GLRotate& glRot) {
      glRotated(glRot.angle, glRot.x, glRot.y, glRot.z);
    }
  }, rot_);

  glBegin(GL_TRIANGLES);
  for (const TriIndices& is : indices_) {
    for (std::size_t i = 0; i < 3; i++) {
      GLuint index = is.is[i];
      const auto& p = verts_[index];
      glNormal3f(p.normal[0], p.normal[1], p.normal[2]);
      glVertex3f(p.vert[0], p.vert[1], p.vert[2]);
    }
  }
  glEnd();

  glPopMatrix();
}

void ObjectModel::pos(math::Vector<3> pos) { pos_ = pos; }

auto ObjectModel::pos() const -> const math::Vector<3>& { return pos_; }

void ObjectModel::set_rotate_dcm(const math::Matrix<4, 4>& dcm) { rot_ = dcm; }

void ObjectModel::set_rotate_gl(double angle, const math::Vector<3>& axis) {
  rot_ = GLRotate{angle, axis[0], axis[1], axis[2]};
}