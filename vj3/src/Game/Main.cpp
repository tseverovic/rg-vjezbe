#include "Main.hpp"

#include <chrono>
#include <utility>
#include <algorithm>
#include <iostream>
#include <array>
#include <random>
#include <tuple>

#include <GL/glew.h>

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_events.h>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "Physics/All.hpp"

#include "Meshes.hpp"

#include "shaderLoader.hpp"
#include "Log.hpp"
#include "bspline.hpp"


using logger = Log::logger<Log::verbosity::spew>;

using namespace std::literals;

using sys_clock = std::chrono::system_clock;

namespace Game {

std::default_random_engine random_engine{std::random_device{}()};

float mix(float a, float b, float part) { return a * (1.0f - part) + b * part; }

struct object_t {
  Physics::element element;
  Physics::phys_data phys_data;

  bool collides;
};

template <class T>
auto create_buffer_storage(std::vector<GLuint>& buffers, T const& obj,
                           GLenum flags = 0) noexcept {
  buffers.push_back(0);
  glCreateBuffers(1, &buffers.back());

  glNamedBufferStorage(buffers.back(), sizeof(T),
                       reinterpret_cast<const void*>(&obj), flags);

  return buffers.back();
}

auto scale(Physics::box const& b) noexcept {
  return glm::scale(glm::vec3{b.halfwidth, b.halfheight, b.halfdepth});
}

auto scale(Physics::sphere const& s) noexcept {
  return glm::scale(glm::vec3{s.radius, s.radius, s.radius});
}

auto scale(Physics::element const& elem) noexcept {
  return std::visit([](auto const& e) { return scale(e); }, elem);
}

auto model_transform(object_t const& obj) noexcept {
  return glm::translate(obj.phys_data.pos) * glm::mat4_cast(obj.phys_data.rot) *
         scale(obj.element);
}

using path_t = std::pair<std::vector<math::Vector<3>>, glm::quat>;

void programUniform(GLuint program, GLuint location,
                    glm::mat4 const& mat) noexcept {
  glProgramUniformMatrix4fv(program, location, 1, GL_FALSE,
                            glm::value_ptr(mat));
}

struct shader {
  GLuint id;

  static constexpr GLuint light_bind = 0;
  struct alignas(16) light_data {
    using vec3 = glm::vec<3, float, glm::packed>;
    vec3 pos;
    float padding1__;
    vec3 color;
    float padding2__;

    light_data() = default;
    light_data(light_data&&) = default;
    light_data(light_data const&) = default;
    light_data& operator=(light_data&&) = default;
    light_data& operator=(light_data const&) = default;

    light_data(glm::vec3 const& pos, glm::vec3 const& color) noexcept
        : pos{pos}, color{color} {}
  };

  static constexpr GLuint material_bind = 1;
  struct alignas(16) material {
    using vec3 = glm::vec<3, float, glm::packed>;
    vec3 diffuse_color;
    float padding1__;
    vec3 spec_color;
    float padding2__;
    vec3 ambient_color;
    float spec;

    material() = default;
    material(material&&) = default;
    material(material const&) = default;
    material& operator=(material&&) = default;
    material& operator=(material const&) = default;

    material(glm::vec3 const& diffuse_color, glm::vec3 const& spec_color,
             glm::vec3 const& ambient_color, float spec) noexcept
        : diffuse_color{diffuse_color},
          spec_color{spec_color},
          ambient_color{ambient_color},
          spec{spec} {}
  };

  shader() = default;
  shader(shader const&) = default;
  shader(shader&&) = default;
  shader& operator=(shader const&) = default;
  shader& operator=(shader&&) = default;

#define MAKE_UNIFORM_AND_SETTER(name, pos, type) \
  static constexpr GLuint name##_loc = pos;      \
  void name(type const& v) const noexcept { programUniform(id, name##_loc, v); }

  MAKE_UNIFORM_AND_SETTER(MV, 0, glm::mat4);
  MAKE_UNIFORM_AND_SETTER(MV_inv_transpose, 1, glm::mat4);
  MAKE_UNIFORM_AND_SETTER(P, 2, glm::mat4);

#undef MAKE_UNIFORM_AND_SETTER
  shader(GLuint id) : id{id} {}
};

struct state {
  std::vector<object_t> objects;
  std::vector<path_t> object_paths;

  std::vector<Meshes::object_draw_data> draw_data;

  std::vector<GLuint> vaos;
  std::vector<GLuint> buffers;

  shader global_shader;

  int width;
  int height;
  bool resized = true;

  bool quit = false;

  glm::vec3 camera_rot{0.0f, 0.0f, 0.0f};
  float camera_dist = 10.0f;
  float camera_fov = 70.0f;

  GLuint red_material;
  GLuint green_material;

  GLuint global_light;

  // use indirect buffers instead
  Meshes::object_draw_data box_draw_data;
  Meshes::object_draw_data sphere_draw_data;

  const Uint8* keystates = SDL_GetKeyboardState(nullptr);

  ~state() noexcept {
    glDeleteVertexArrays(vaos.size(), vaos.data());
    glDeleteBuffers(buffers.size(), buffers.data());
    glDeleteProgram(global_shader.id);
  }
};

auto vec3_cast(math::Vector<3> vec) noexcept -> glm::vec3 {
  return glm::vec3{vec[0], vec[1], vec[2]};
}

auto move_phys_data(Physics::phys_data original, path_t const& path,
                    double d) noexcept {
  double newt = std::fmod(original.t + d, path.first.size() - 3);
  return Physics::phys_data{vec3_cast(*bspline_vector(path.first, newt)),
                            original.rot * path.second, newt};
}

void move_objects(std::vector<object_t>& objects,
                  std::vector<path_t>& object_paths, double d) noexcept {
  std::transform(objects.begin(), objects.end(), object_paths.begin(),
                 objects.begin(), [=](auto const& original, auto const& path) {
                   return object_t{original.element,
                                   move_phys_data(original.phys_data, path, d),
                                   false};
                 });
}

constexpr auto element_string(Physics::box) noexcept { return "box"sv; }
constexpr auto element_string(Physics::sphere) noexcept { return "sphere"sv; }
constexpr auto element_string(Physics::triangle) noexcept {
  return "triangle"sv;
}

constexpr auto element_string(Physics::element const& elem) noexcept {
  return std::visit(
      [](auto const& elem_real) { return element_string(elem_real); }, elem);
}

void collide_objects(std::vector<object_t>& objects) noexcept {
  for (int i = 0; i < objects.size(); ++i) {
    object_t& i_obj = objects[i];
    std::string_view obj_name = element_string(i_obj.element);
    for (int j = i + 1; j < objects.size(); ++j) {
      object_t& j_obj = objects[j];
      bool objects_collide = std::visit(
          [&](const auto& i_elem, const auto& j_elem) {
            return collides(i_elem, i_obj.phys_data, j_elem, j_obj.phys_data);
          },
          i_obj.element, j_obj.element);

      if (objects_collide) {
        i_obj.collides = true;
        j_obj.collides = true;
        std::cout << "Objects " << i << "(" << obj_name << ") & " << j << " ("
                  << element_string(j_obj.element) << ") collide\n";
      }
    }
  }
}

void draw_scene(state& game_state) noexcept {
  if (game_state.resized) {
    game_state.resized = false;
    glViewport(0, 0, game_state.width, game_state.height);
  }

  glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::quat q_rot{game_state.camera_rot};
  auto cam_pos =
      glm::rotate(q_rot, glm::vec3{0.0f, 0.0f, game_state.camera_dist});

  // TODO: view vector should be q_rot rotated around ITS y-axis.
  /*
  auto V = glm::mat4_cast(q_rot *
                          glm::quat{(float)M_PI, glm::vec3{0.0f, 1.0f, 0.0f}}) *
           glm::translate(cam_pos);

   */
  auto V = glm::lookAt(cam_pos, glm::vec3{0.0f, 0.0f, 0.0f},
                       glm::vec3{0.0f, 1.0f, 0.0f});

  shader::light_data new_light{
      glm::vec3{V * glm::vec4{5.0f, 5.0f, 5.0f, 1.0f}},
      glm::vec3{1.0f, 1.0f, 1.0f}};

  glNamedBufferSubData(game_state.global_light, 0, sizeof(new_light),
                       reinterpret_cast<const void*>(&new_light));
  glBindBufferBase(GL_UNIFORM_BUFFER, shader::light_bind,
                   game_state.global_light);

  auto P =
      glm::perspective(glm::radians(game_state.camera_fov),
                       (float)game_state.width / game_state.height, 1.0f, 1e5f);

  shader const& shader = game_state.global_shader;
  glUseProgram(shader.id);
  shader.P(P);
  for (int i = 0; i < game_state.objects.size(); ++i) {
    auto const& obj = game_state.objects[i];
    auto const& draw_data = game_state.draw_data[i];

    glBindVertexArray(draw_data.vao);
    glBindBufferBase(
        GL_UNIFORM_BUFFER, shader::material_bind,
        obj.collides ? game_state.red_material : game_state.green_material);

    glm::mat4 MV = V * model_transform(obj);
    // there is a faster way to invert this matrix
    glm::mat4 MV_inv_transpose = glm::transpose(glm::inverse(MV));
    shader.MV(MV);
    shader.MV_inv_transpose(MV_inv_transpose);

    glDrawElements(GL_TRIANGLES, draw_data.verts, GL_UNSIGNED_INT, nullptr);
  }
}

auto random_box() noexcept {
  std::uniform_real_distribution dist{0.5f, 2.0f};
  return Physics::box{dist(random_engine), dist(random_engine),
                      dist(random_engine)};
}

auto random_sphere() noexcept {
  float radius = std::uniform_real_distribution{0.1f, 0.7f}(random_engine);
  return Physics::sphere{radius};
}

/*
auto random_triangle() noexcept
    -> std::tuple<Physics::element, GLuint, std::vector<GLuint>, GLuint> {
  std::uniform_real_distribution vert_dist{-0.5f, 0.5f};

  auto random_point = [&] {
    return glm::vec3{vert_dist(random_engine), vert_dist(random_engine),
                     vert_dist(random_engine)};
  };

  Physics::triangle tri{random_point(), random_point(), random_point()};

  std::vector<glm::vec3> verts{tri.p1, tri.p2, tri.p3};

  // visible from both sides
  std::vector<GLuint> indices{0, 1, 2, 0, 2, 1};
  auto&& [vao, vbos] = send_mesh(std::move(verts), std::move(indices));
  return {tri, vao, std::move(vbos), 6};
}
*/

auto random_path() noexcept {
  auto points = std::uniform_int_distribution{5, 10}(random_engine);
  std::uniform_real_distribution path_points_distribution{-10.0, 10.0};
  std::vector<math::Vector<3>> path;
  path.reserve(points);

  for (int i = 0; i < points; ++i) {
    path.push_back(math::Vector<3>{path_points_distribution(random_engine),
                                   path_points_distribution(random_engine),
                                   path_points_distribution(random_engine)});
  }
  return path;
}

auto random_rotation() noexcept {
  std::uniform_real_distribution angle_distribution{-1.0f / 180.0f * M_PI,
                                                    1.0f / 180.0f * M_PI};

  return glm::quat{glm::vec3{angle_distribution(random_engine),
                             angle_distribution(random_engine),
                             angle_distribution(random_engine)}};
}

void add_object(state& game_state, Physics::element elem,
                Meshes::object_draw_data draw_data) {
  auto&& path = random_path();
  game_state.objects.push_back(
      {std::move(elem),
       Physics::phys_data{vec3_cast(*bspline_vector(path, 0.0)),
                          glm::quat{glm::vec3{0.0f, 0.0f, 0.0f}}, 0.0},
       false});
  game_state.object_paths.push_back({std::move(path), random_rotation()});
  game_state.draw_data.push_back(std::move(draw_data));
}

void process_events(state& game_state) noexcept {
  SDL_Event ev;
  while (SDL_PollEvent(&ev)) {
    switch (ev.type) {
      case SDL_KEYUP: {
        SDL_KeyboardEvent ke = ev.key;
        switch (ke.keysym.sym) {
          case SDLK_ESCAPE:
            game_state.quit = true;
            break;
        }
      } break;
      case SDL_WINDOWEVENT: {
        SDL_WindowEvent we = ev.window;
        switch (we.event) {
          case SDL_WINDOWEVENT_CLOSE:
            game_state.quit = true;
            break;
          case SDL_WINDOWEVENT_RESIZED:
            game_state.width = we.data1;
            game_state.height = we.data2;
            game_state.resized = true;
            break;
        }
      }
    }
  }

  if (game_state.keystates[SDL_SCANCODE_LEFT]) {
    game_state.camera_rot.y -= 0.02;
  }
  if (game_state.keystates[SDL_SCANCODE_RIGHT]) {
    game_state.camera_rot.y += 0.02;
  }
  if (game_state.keystates[SDL_SCANCODE_UP]) {
    game_state.camera_rot.x -= 0.02;
  }
  if (game_state.keystates[SDL_SCANCODE_DOWN]) {
    game_state.camera_rot.x += 0.02;
  }
}

auto create_material(glm::vec3 color, float diffuse_factor, float spec_factor,
                     float ambient_factor, float spec_exp,
                     bool metallic = false) noexcept {
  return shader::material{
      color * diffuse_factor,
      (metallic ? color : glm::vec3{1.0f, 1.0f, 1.0f}) * spec_factor,
      color * ambient_factor, spec_exp};
}

void main(SDL_Window* window) noexcept {
  state game_state;
  logger::spew() << "Constructed game_state";

  SDL_GetWindowSize(window, &game_state.width, &game_state.height);

  if (auto shader_o = loadShader("shaders/vertex.glsl", "shaders/pixel.glsl")) {
    game_state.global_shader = {*shader_o};
  } else {
    logger::error() << "Failed to open shaders!";
    std::exit(-1);
  }

  game_state.box_draw_data = Meshes::send_mesh(
      game_state.vaos, game_state.buffers, Meshes::box_mesh());
  game_state.sphere_draw_data = Meshes::send_mesh(
      game_state.vaos, game_state.buffers, Meshes::sphere_mesh(20, 20));

  glm::vec3 red{1.0f, 0.0f, 0.0f};
  glm::vec3 green{0.0f, 1.0f, 0.0f};

  float diffuse_factor = 0.8f;
  float specular_factor = 1.0f;
  float ambient_factor = 0.2f;
  float specular_exp = 128.0f;

  game_state.red_material = create_buffer_storage(
      game_state.buffers, create_material(red, diffuse_factor, specular_factor,
                                          ambient_factor, specular_exp));
  game_state.green_material = create_buffer_storage(
      game_state.buffers,
      create_material(green, diffuse_factor, specular_factor, ambient_factor,
                      specular_exp));

  game_state.global_light = create_buffer_storage(
      game_state.buffers, shader::light_data{}, GL_DYNAMIC_STORAGE_BIT);

  for (int i = 0; i < 5; ++i) {
    add_object(game_state, random_box(), game_state.box_draw_data);
    logger::debug() << "Added a box";
  }

  for (int i = 0; i < 5; ++i) {
    add_object(game_state, random_sphere(), game_state.sphere_draw_data);
    logger::debug() << "Added a sphere";
  }

  /*
  for (int i = 0; i < 20; ++i) {
    add_object(game_state, random_triangle());
  }
  */

  int flush_timer = 0;
  do {
    move_objects(game_state.objects, game_state.object_paths, 0.01);

    collide_objects(game_state.objects);
    draw_scene(game_state);

    SDL_GL_SwapWindow(window);

    process_events(game_state);

    if (++flush_timer == 30) {
      flush_timer = 0;
      std::cout << std::flush;
    }
  } while (!game_state.quit);
}

}  // namespace Game