#pragma once

#include <glm/fwd.hpp>
#include <glm/gtc/quaternion.hpp>

#include "box.hpp"
#include "sphere.hpp"

namespace Physics {

struct phys_data {
  glm::vec3 pos;
  glm::quat rot;
  double t;
};


bool collides(sphere const& s1, phys_data const& d1,
              sphere const& s2, phys_data const& d2) noexcept;


bool collides(Physics::sphere const& s, phys_data const& d1,
              Physics::box const& b, phys_data const& d2) noexcept;

bool collides(Physics::box const& b, phys_data const& d1,
              Physics::sphere const& s, phys_data const& d2) noexcept;

bool collides(Physics::box const& b1, phys_data const& d1,
              Physics::box const& b2, phys_data const& d2) noexcept;
}