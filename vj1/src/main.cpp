#include <fstream>
#include <iostream>
#include <iterator>
#include <utility>

#include <cstdlib>

#include "bspline.hpp"
#include "ObjectModel.hpp"

using namespace std::string_literals;

ObjectModel object;
std::vector<math::Vector<3>> curve;

struct Line {
  math::Vector<3> begin, end;

  void draw() {
    glBegin(GL_LINES);

    glVertex2dv(std::addressof(begin[0]));
    glVertex2dv(std::addressof(end[0]));

    glEnd();
  }
};

std::vector<math::Vector<3>> curve_points;
std::vector<math::Vector<3>> curve_tangents;

struct {
  int width;
  int height;
} window;

[[noreturn]] void usage_error(char* prog) {
  std::cerr << "usage: " << prog << " object_file curve_file\n";
  std::exit(EXIT_FAILURE);
}

    [[noreturn]] void cant_open(char* file) {
  std::cerr << "Can't open " << file << "!\n";
  std::exit(EXIT_FAILURE);
}

[[noreturn]] void bad_file(char* file) {
  std::cerr << "Bad file " << file << "!\n";
  std::exit(EXIT_FAILURE);
}

void load_object(char* fname) {
  std::ifstream file{fname};
  if (!file) {
    cant_open(fname);
  }

  if ((file >> object).bad()) {
    bad_file(fname);
  }
}

constexpr int POINTS_PER_SEGMENT = 10;

void load_curve(char* fname) {
  std::ifstream file{fname};
  if (!file) {
    cant_open(fname);
  }
  std::copy(std::istream_iterator<math::Vector<3>>(file),
            std::istream_iterator<math::Vector<3>>(),
            std::back_inserter(curve));

  if (file.bad() || curve.size() < 4) {
    bad_file(fname);
  }

  int segments = curve.size() - 3;
  int points = POINTS_PER_SEGMENT * segments;

  curve_points.reserve(points);
  curve_tangents.reserve(points);

  for (int i = 0; i < points; i++) {
    curve_points.emplace_back(
        *bspline_vector(curve, (double)i / POINTS_PER_SEGMENT));
    curve_tangents.emplace_back(
        curve_points.back() +
        bspline_tangent(curve, (double)i / POINTS_PER_SEGMENT)
            .value()
            .normalized());
  }
}

void draw_curve() {
  glDisable(GL_LIGHTING);

  glBegin(GL_LINE_STRIP);
  glColor3f(1.0f, 0.0f, 0.0f);
  for (std::size_t i = 0; i < curve_points.size(); i++) {
    glVertex3dv(std::addressof(curve_points[i][0]));
  }
  glEnd();

  glBegin(GL_LINES);
  glColor3f(0.0f, 1.0f, 0.0f);
  for (std::size_t i = 0; i < curve_points.size(); i++) {
    glVertex3dv(std::addressof(curve_points[i][0]));
    glVertex3dv(std::addressof(curve_tangents[i][0]));
  }
  glEnd();

  glEnable(GL_LIGHTING);
}

void reshape(int x, int y) {
  window = {x, y};
  glutPostRedisplay();
}

double fov = 70;

bool use_dcm = false;

bool free_camera = true;

int front_face = GL_CCW;

math::Vector<3> camera_pos_f{2.0, 2.0, 2.0};

auto get_camera_pos() -> math::Vector<3> {
  if (free_camera) {
    return camera_pos_f;
  } else {
    return camera_pos_f + object.pos();
  }
}

void display() {
  glViewport(0, 0, window.width, window.height);

  glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  auto& pos = object.pos();
  math::Vector<3> camera_pos = get_camera_pos();
  gluPerspective(fov, (double)window.width / window.height, 1.0, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  gluLookAt(camera_pos[0], camera_pos[1], camera_pos[2], pos[0], pos[1], pos[2],
            0.0, 1.0, 0.0);

  draw_curve();

  object.draw();

  glutSwapBuffers();
}

constexpr int TIMESTEP = 16;
constexpr double T_PER_SEC = 1.0;

void special(int key, int, int) {
  switch (key) {
    case GLUT_KEY_UP:
      camera_pos_f[2] += 0.2;
      break;
    case GLUT_KEY_DOWN:
      camera_pos_f[2] -= 0.2;
      break;
    case GLUT_KEY_LEFT:
      camera_pos_f[0] -= 0.2;
      break;
    case GLUT_KEY_RIGHT:
      camera_pos_f[0] += 0.2;
      break;
  }
}

void keys(unsigned char c, int, int) {
  switch (c) {
    case ' ':
      camera_pos_f[1] += 0.2;
      break;
    case 'c':
      camera_pos_f[1] -= 0.2;
      break;
    case 'x':
      use_dcm = !use_dcm;
      break;
    case 'z':
      free_camera = !free_camera;
      break;
    case '+':
      fov = std::min(150.0, fov * 1.1);
      break;
    case '-':
      fov = std::max(5.0, fov * 0.9);
      break;
    case 'f':
      front_face = front_face == GL_CCW ? GL_CW : GL_CCW;
      glFrontFace(front_face);
      break;
  }
}

void timer(int current_timepoint) {
  double t_cur = current_timepoint / 1000.0 * T_PER_SEC;
  auto pos_o = bspline_vector(curve, t_cur);
  if (!pos_o) {
    current_timepoint = 0;
    t_cur = 0.0;
    pos_o = bspline_vector(curve, t_cur);
  }
  object.pos(*pos_o);

  if (use_dcm) {
    auto w = bspline_tangent(curve, t_cur)->normalized();
    auto u = w.x(bspline_2deriv(curve, t_cur)->normalized()).normalized();
    auto v = w.x(u).normalized();

    object.set_rotate_dcm(math::Matrix<4, 4>{u[0], v[0], w[0], 0.0, u[1], v[1],
                                             w[1], 0.0, u[2], v[2], w[2], 0.0,
                                             0.0, 0.0, 0.0, 1.0}
                              .inverted()
                              .second);
  } else {
    auto e = bspline_tangent(curve, t_cur)->normalized();
    constexpr math::Vector<3> s{0.0, 0.0, 1.0};
    auto os = s.x(e);
    double phi = std::acos(s.cosine(e)) * 180 / M_PI;

    object.set_rotate_gl(phi, os);
  }

  glutTimerFunc(TIMESTEP, timer, current_timepoint + TIMESTEP);
  glutPostRedisplay();
}

auto main(int argc, char* argv[]) -> int {
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(window.width, window.height);
  glutInit(&argc, argv);

  if (argc != 3) {
    usage_error(argv[0]);
  }

  load_object(argv[1]);
  load_curve(argv[2]);

  std::string window_name = "LV1 - model "s + argv[1] + ", curve "s + argv[2];
  glutCreateWindow(window_name.c_str());

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(front_face);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);

  glShadeModel(GL_SMOOTH);

  glutKeyboardFunc(keys);
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutSpecialFunc(special);
  timer(0);

  glutMainLoop();

  return EXIT_SUCCESS;
}
