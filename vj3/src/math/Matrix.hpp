/**
 * \file Matrix.hpp
 * \brief Header file of the Matrix class.
 * 
 * \author Tomislav Severović
 */

#ifndef math_Matrix_HPP
#define math_Matrix_HPP

#include <array>
#include <iosfwd>
#include <utility>
#include <numeric>

#include <cmath>

#include "Vector.hpp"

/*
 * Classes in this file use CRTP to achieve static polymorphism;
 * look it up if you're interested in modifying or extending them.
 */

/**
 * \brief Contains mathematical utilities.
 */
namespace math {

namespace detail {

// forward declarations

template <std::size_t N, std::size_t M, class Mat>
class MatrixBase;

template <std::size_t N, std::size_t M, class Mat>
class MutableMatrixBase;

} // namespace detail

template <std::size_t N, std::size_t M>
class Matrix;

namespace detail {

/**
 * \brief Base class of all matrices.
 *
 * Defines most operations on matrices.
 *
 * \author Tomislav Severović
 */
template <std::size_t N,  ///< Number of rows.
          std::size_t M,  ///< Number of columns.
          class Mat       ///< Subclass.
          >
class MatrixBase {
 protected:
  // convenience typedef
  using CMat = Matrix<N, M>;

  /**
   * \brief Contains results of LUP decomposition.
   */
  struct LupDecomp {
    bool singular = false;         ///< Is the matrix singular.
    bool det_negative = false;     ///< Is the determinant negated (number of
                                   ///< permutations % 2 != 0).
    CMat lu;                       ///< Decomposition of matrix.
    std::array<std::size_t, N> p;  ///< Permutation list.
    constexpr LupDecomp() { std::iota(p.begin(), p.end(), 0); }

    // accessing permuted table element i,j -> mat(p[i], j)
  };

  /**
   * \brief Get \p *this as reference to subclass.
   *
   * \return Equivalent to <tt>static_cast<const Mat&>(*this)</tt>.
   */
  constexpr const Mat& _refine() const {
    return static_cast<const Mat&>(*this);
  }

  /**
   * \brief Decomposes the matrix into LUP components.
   *
   * LUP decomposition decomposes a matrix \e A into 3 components, a lower
   * triangular matrix \e L, an upper triangular matrix \e U, and a row
   * permutation matrix \e P (in this implementation it's a list of row
   * permutations instead), where <em> PA = LU <\em>. All square matrices can be
   * factorized in this form.
   *
   * Currently it is only used to compute matrix inverse and determinant, and so
   * it does not fully decompose the matrix if it determines that it is singular
   * (or close, to avoid numerical instability).
   */
  template <std::size_t N1 = N, std::size_t M1 = M>
  constexpr std::enable_if_t<N1 == M1, LupDecomp> _lup_decompose() const {
    // conditionally compiled

    /*
     * After the process, the lu matrix contains the L and U components, L being
     * in the lower triangle and U in the upper. The diagonal is U's diagonal,
     * L's diagonal is all 1s.
     */
    LupDecomp result;
    result.lu = CMat(_refine());
    auto& p = result.p;
    auto& lu = result.lu;

    for (std::size_t i = 0; i < N - 1; i++) {
      double max_col = std::fabs(lu(p[i], i));
      std::size_t max_col_ix = i;

      for (std::size_t j = i + 1; j < N; j++) {
        std::size_t j_row = p[j];
        double abs_col = 0.0;
        if ((abs_col = std::fabs(lu(j_row, i))) > max_col) {
          max_col = abs_col;
          max_col_ix = j;
        }
      }

      if (max_col < std::numeric_limits<double>::epsilon()) {
        // column is all zeroes
        result.singular = true;
        return result;
      }

      if (max_col_ix != i) {
        // move max row to i-th row
        // swap rows -> negate determinant
        std::swap(p[i], p[max_col_ix]);
        result.det_negative = !result.det_negative;
      }

      std::size_t i_row = p[i];
      for (std::size_t j = i + 1; j < N; j++) {
        std::size_t j_row = p[j];
        lu(j_row, i) /= lu(i_row, i);

        for (std::size_t k = i + 1; k < N; k++) {
          lu(j_row, k) -= lu(j_row, i) * lu(i_row, k);
        }
      }
    }

    // check last diagonal element
    if (std::fabs(lu(p[N - 1], N - 1)) <
        std::numeric_limits<double>::epsilon()) {
      result.singular = true;
    }

    return result;
  }
  constexpr MatrixBase() = default;

 public:
  /// Returns number of rows.
  constexpr std::size_t rows() const { return N; }
  /// Returns number of columns.
  constexpr std::size_t columns() const { return M; }

  /**
   * \brief Matrix addition.
   *
   * \param[in] mat Second term.
   * \tparam MatArg Type of second term.
   * \return Sum of two matrices.
   */
  template <class MatArg>
  constexpr CMat operator+(MatArg mat) const {
    static_assert(N == mat.rows() && M == mat.columns(),
      "Number of columns and rows don't match");
    return CMat(_refine()) += mat;
  }

  /**
   * \brief Matrix subtraction.
   *
   * \param[in] mat Subtrahend.
   * \tparam MatArg Type of subtrahend.
   * \return Difference of the two matrices.
   */
  template <class MatArg>
  constexpr CMat operator-(MatArg mat) const {
    static_assert(N == mat.rows() && M == mat.columns(),
      "Number of columns and rows don't match");
    return CMat(_refine()) -= mat;
  }

  /**
   * \brief Matrix multiplication.
   *
   * \param[in] mat Other matrix.
   * \tparam P Number of columns of other matrix.
   * \tparam MatArg Type of argument.
   * \return Product of two matrices.
   */
  template <class MatArg, class = std::enable_if_t<std::is_class_v<MatArg>>>
  constexpr auto operator*(MatArg mat) const {
    static_assert(mat.rows() == M, "Invalid matrix widths in function");

    Matrix<N, mat.columns()> result;

    for (std::size_t i = 0; i < result.rows(); i++) {
      for (std::size_t j = 0; j < result.columns(); j++) {
        for (std::size_t k = 0; k < M; k++) {
          result(i, j) += _refine()(i, k) * mat(k, j);
        }
      }
    }

    return result;
  }

  constexpr auto operator*(double x) const {
    Matrix<N, M> result;

    for (std::size_t i = 0; i < result.rows(); i++) {
      for (std::size_t j = 0; j < result.columns(); j++) {
        result(i, j) = _refine()(i, j) * x;
      }
    }

    return result;
  }

  friend constexpr auto operator*(double x, const Mat& m) -> Matrix<N, M> {
    return m * x;
  }

  /**
   * \brief Returns the determinant of the matrix.
   *
   * \return Determinant of matrix.
   */
  template <std::size_t N1 = N, std::size_t M1 = M>
  constexpr std::enable_if_t<N1 == M1, double> determinant() const {
    // conditionally compiled
    auto decomp = _lup_decompose();

    if (decomp.singular) {
      return 0.0;
    }

    double det = decomp.lu(decomp.p[0], 0);
    for (std::size_t i = 1; i < N; i++) {
      det *= decomp.lu(decomp.p[i], i);
    }
    return decomp.det_negative ? -det : det;
  }

  /**
   * \brief Returns the inverted matrix.
   * 
   * Returns an <tt>std::pair<\tt>, where the first element indicates if the
   * matrix is invertible, and the second element is the inverted matrix
   * if it is not singular and is some indeterminate value otherwise.
   * 
   * \return <tt>std::pair</tt> containing results of inversion.
   */
  template <std::size_t N1 = N, std::size_t M1 = M>
  constexpr std::enable_if_t<N1 == M1, std::pair<bool, CMat>> inverted() const {
    // conditionally compiled
    auto decomp = _lup_decompose();
    if (decomp.singular) {
      return {false, {}};
    }

    CMat result;
    for (std::size_t j = 0; j < N; j++) {
      for (std::size_t i = 0; i < N; i++) {
        std::size_t i_row = decomp.p[i];
        result(i, j) = i_row == j ? 1.0 : 0.0;

        for (std::size_t k = 0; k < i; k++) {
          result(i, j) -= decomp.lu(i_row, k) * result(k, j);
        }
      }

      for (std::size_t i = N - 1; i > 0; i--) {
        std::size_t i_row = decomp.p[i];
        for (std::size_t k = i + 1; k < N; k++) {
          result(i, j) -= decomp.lu(i_row, k) * result(k, j);
        }
        result(i, j) /= decomp.lu(i_row, i);
      }

      std::size_t z_row = decomp.p[0];
      for (std::size_t k = 1; k < N; k++) {
        result(0, j) -= decomp.lu(z_row, k) * result(k, j);
      }
      result(0, j) /= decomp.lu(z_row, 0);
    }

    return {true, result};
  }

  /**
   * \brief Returns a transposed matrix.
   */
  constexpr Matrix<M, N> transposed() const {
    Matrix<M, N> result;

    for (std::size_t i = 0; i < M; i++) {
      for (std::size_t j = 0; j < N; j++) {
        result(i, j) = _refine()(j, i);
      }
    }

    return result;
  }

  /**
   * \brief Prints the matrix into the given ostream.
   * 
   * \param[out] os <tt>std::ostream</tt> to write to.
   * \param[in] mat Matrix to write out.
   * \return \p os
   */
  friend std::ostream& operator<<(std::ostream& os, const Mat& mat) {
    if  (mat.rows() == 0 || mat.columns() == 0) {
      return os;
    }

    for (std::size_t i = 0; i < mat.rows() - 1; i++) {
      for (std::size_t j = 0; j < mat.columns() - 1; j++) {
        os << mat(i, j) << ' ';
      }
      os << mat(i, mat.columns() - 1) << '\n';
    }

    for (std::size_t j = 0; j < mat.columns() - 1; j++) {
      os << mat(mat.rows() - 1, j) << ' ';
    }

    return os << mat(mat.rows() - 1, mat.columns() - 1);
  }

  /**
   * \brief Compares two matrices for equality.
   * 
   * \param[in] mat Matrix to compare with \p *this.
   * \tparam MatArg Type of matrix.
   * \return \p true if the matrices are equal, \p false otherwise.
   * \note Compares floating point values.
   */
  template <class MatArg>
  bool operator==(const MatArg& mat) const {
    if (N != mat.columns() || M != mat.rows()) return false;

    for (std::size_t i = 0; i < N; i++) {
      for (std::size_t j = 0; j < M; j++) {
        if (_refine()(i, j) != mat(i, j)) return false;
      }
    }

    return true;
  }
}; // class MatrixBase

/**
 * \brief Base class of mutable matrices.
 */
template <std::size_t N, ///< Number of rows.
          std::size_t M, ///< Number of columns.
          class Mat      ///< Derived Matrix class.
          >
class MutableMatrixBase 
  : public MatrixBase<N, M, Mat> {
  // convenience typedef
  using CMat = Matrix<N, M>;

  /**
   * \brief Get \p *this as reference to subclass.
   *
   * \return Equivalent to <tt>static_cast<Mat&>(*this)</tt>.
   */
  constexpr Mat& _refine() { return static_cast<Mat&>(*this); }

 protected:
  constexpr MutableMatrixBase() = default;

 public:
  // TODO: pass by value instead of reference for small matrices

  /**
   * \brief In place matrix addition.
   * \note The matrix is modified by this operation.
   *
   * \param[in] mat Second term.
   * \tparam MatArg Type of second term.
   * \return Reference to \p *this.
   */
  template <class MatArg>
  constexpr Mat& operator+=(MatArg mat) {
    static_assert(N == mat.rows() && M == mat.columns(),
      "Number of columns and rows don't match");

    for (std::size_t i = 0; i < N; i++) {
      for (std::size_t j = 0; j < M; j++) {
        _refine()(i, j) += mat(i, j);
      }
    }
    return _refine();
  }

  /**
   * \brief In place matrix subtraction.
   * \note The matrix is modified by this operation.
   *
   * \param[in] mat Subtrahend.
   * \tparam MatArg Type of subtrahend.
   * \return Reference to \p *this.
   */
  template <class MatArg>
  constexpr Mat& operator-=(MatArg mat) {
    static_assert(N == mat.rows() && M == mat.columns(),
      "Number of columns and rows don't match");

    for (std::size_t i = 0; i < N; i++) {
      for (std::size_t j = 0; j < M; j++) {
        _refine()(i, j) -= mat(i, j);
      }
    }
    return _refine();
  }


  /**
   * \brief Reads a matrix from an <tt>std::istream</tt>.
   *
   * \param[in] is <tt>std::istream</tt> to read from.
   * \param[out] v Vector to update.
   * \return \p is
   */
  friend std::istream& operator>>(std::istream& is, Mat& mat) {
    if (mat.rows() == 0 || mat.columns() == 0) {
      return is;
    }

    for (std::size_t i = 0; i < mat.rows(); i++) {
      for (std::size_t j = 0; j < mat.columns(); j++) {
        is >> mat(i, j);
        /* Not like in documentation: this one does not want pipes in input. */
      }
    }

    return is;
  }
}; // class MutableMatrixBase

}  // namespace detail

/**
 * \brief Matrix class.
 *
 * A concrete implementation of a matrix, supporting all the basic operations
 * involving matrices.
 *
 * \author Tomislav Severović
 */
template <std::size_t N,  ///< Number of rows.
          std::size_t M   ///< Number of columns.
          >
class Matrix
  : public detail::MutableMatrixBase<N, M, Matrix<N, M>> {

  using storage_type = std::array<double, N * M>;
  /**
   * \brief The elements store.
   */
  storage_type _elements = {};

 public:
  constexpr Matrix() = default;
  constexpr Matrix(const Matrix&) = default;

  /**
   * \brief Brace initialization constructor.
   *
   * \param[in] args... Elements of matrix rows.
   * \tparam T... Types of arguments.
   */
  template <class... T>
  constexpr Matrix(T... args) : _elements{{args...}} {}

  constexpr Matrix& operator=(const Matrix&) = default;

  /// Returns const reference to element storage.
  constexpr const storage_type& elements() const { return _elements; }
  /// Returns reference to element storage.
  constexpr storage_type& elements() { return _elements; }

  /**
   * \brief Gets element of matrix at row \p i and column \p j.
   *
   * \param[in] i Row to index.
   * \param[in] j Column to index.
   * \returns Reference to element of matrix.
   * \warning No bounds checking is performed.
   */
  constexpr double& operator()(std::size_t i, std::size_t j) {
    return _elements[i * M + j];
  }

  /**
   * \brief Gets element of matrix at row \p i and column \p j.
   *
   * \param[in] i Row to index.
   * \param[in] j Column to index.
   * \returns Const reference to element of matrix.
   * \warning No bounds checking is performed.
   */
  constexpr const double& operator()(std::size_t i, std::size_t j) const {
    return _elements[i * M + j];
  }
};  // class Matrix


template <std::size_t N, std::size_t M>
class MatrixView;

/*
 * \brief A read-only view of a matrix which transposes the given matrix.
 *
 * It is a non-owning view of the matrix, and accessing its elements
 * after the original matrix has expired is undefined behavior.
 */
template <std::size_t N, ///< Number of rows.
          std::size_t M  ///< Number of columns.
          >
class MatrixTransposeView
    : public detail::MatrixBase<N, M, MatrixTransposeView<N, M>> {

  using storage_type = std::array<std::array<double, N>, M>;
  const storage_type* _data =
      nullptr;  ///< Pointer to original.

  /// Initializes the view using the pointer.
  constexpr MatrixTransposeView(const storage_type* data)
    : _data(data) {}
 public:
  constexpr MatrixTransposeView() = default;
  constexpr MatrixTransposeView(const MatrixTransposeView&) = default;

  /**
   * \brief Returns a const reference to the element storage.
   * \note The elements in storage are not transposed.
   */
  constexpr const storage_type& elements() const { return *_data; }

  /**
   * \brief Gets element of matrix at row \p i and column \p j.
   *
   * \param[in] i Row to index.
   * \param[in] j Column to index.
   * \return  Const reference to element of matrix.
   * \warning No bounds checking is performed.
   */
  constexpr const double& operator()(std::size_t i, std::size_t j) const {
    return (*_data)[j][i];
  }

  template <std::size_t N1, std::size_t M1>
  friend constexpr MatrixTransposeView<N1, M1>
  transposed(const MatrixView<M1, N1>& src);

  /**
   * \brief Returns a transposing view of the matrix.
   * 
   * \param[in] mat Original matrix.
   * \return Transposing view of matrix.
   * \note It is the programmer's responsibility to ensure the view is not used
   *       after the destruction of the original matrix.
   */
  template <class MatArg>
  friend constexpr auto transposed(const MatArg& mat) {
    return MatrixTransposeView<mat.rows(), mat.columns()> { 
      std::addressof(mat.elements())
    };
  }
};


/*
 * \brief A read-only view of a matrix.
 *
 * It is a non-owning view of the matrix, and accessing its elements
 * after the original matrix has expired is undefined behavior.
 */
template <std::size_t N, ///< Number of rows.
          std::size_t M  ///< Number of columns.
          >
class MatrixView
    : public detail::MatrixBase<N, M, MatrixTransposeView<N, M>> {
  using storage_type = std::array<std::array<double, M>, N>;
  const storage_type* _data =
      nullptr;  ///< Pointer to original.

  constexpr MatrixView(const storage_type* data)
    : _data(data) {}

 public:
  constexpr MatrixView() = default;
  constexpr MatrixView(const MatrixView&) = default;

  /// Returns a const reference to the element storage.
  constexpr const storage_type& elements() const { return *_data; }

  /**
   * \brief Gets element of matrix at row \p i and column \p j.
   *
   * \param[in] i Row to index.
   * \param[in] j Column to index.
   * \return  Const reference to element of matrix.
   * \warning No bounds checking is performed.
   */
  constexpr const double& operator()(std::size_t i, std::size_t j) const {
      return (*_data)[i][j];
  }

  /**
   * \brief Transposes the matrix view.
   * 
   * \param[in] mat Original matrix view.
   * \return Transposed view.
   * \note It is the programmer's responsibility to ensure the view is not used
   *       after the destruction of the original matrix.
   */
  template <std::size_t N1, std::size_t M1>
  friend constexpr MatrixView<N1, M1>
  transposed(const MatrixTransposeView<M1, N1>& src) {
    return { src._data };
  }


  /**
   * \brief Transposes the matrix view.
   * 
   * \param[in] mat Original matrix view.
   * \return Transposed view.
   * \note It is the programmer's responsibility to ensure the view is not used
   *       after the destruction of the original matrix.
   */
  template <std::size_t N1, std::size_t M1>
  friend constexpr MatrixTransposeView<N1, M1>
  transposed(const MatrixView<M1, N1>& src) {
    return { src._data };
  }
};

// conversions to Matrix

/**
 * \brief Converts a vector into a row matrix.
 * 
 * \param[in] vec Vector to convert.
 * \return Row matrix form of vector.
 */
template <class VecArg>
constexpr auto row_matrix(VecArg vec) {
  Matrix<1, vec.dimension()> result;
  for (std::size_t i = 0; i < vec.dimension(); i++) {
    result(0, i) = vec[i];
  }
  return result;
}

/**
 * \brief Converts a vector into a column matrix.
 * 
 * \param[in] vec Vector to convert.
 * \return Column matrix form of vector.
 */
template <class VecArg>
constexpr auto column_matrix(VecArg vec) {
  Matrix<vec.dimension(), 1> result;
  for (std::size_t i = 0; i < vec.dimension(); i++) {
    result(i, 0) = vec[i];
  }
  return result;
}


// conversions from Matrix

/**
 * \brief Converts a column matrix to a vector.
 * 
 * \param[in] mat Matrix to convert.
 * \return Vector of column matrix.
 */
template <std::size_t N,
          template <std::size_t, std::size_t> class MatArg>
constexpr Vector<N> to_vector(MatArg<N, 1> mat) {
  Vector<N> result;
  for (std::size_t i = 0; i < N; i++) {
    result[i] = mat(i, 0);
  }
  return result;
}

/**
 * \brief Converts a row matrix to a vector.
 * 
 * \param[in] mat Matrix to convert.
 * \return Vector of row matrix.
 */
template <std::size_t N,
          template <std::size_t, std::size_t> class MatArg>
constexpr Vector<N> to_vector(MatArg<1, N> mat) {
  Vector<N> result;
  for (std::size_t i = 0; i < N; i++) {
    result[i] = mat(0, i);
  }
  return result;
}

// conversions to views are in another header file.

// various special matrixes

/**
 * \brief Constructs a 3D translation matrix.
 * 
 * \param[in] dx, dy, dz Translation of each coordinate.
 * \return Translation matrix.
 */
constexpr Matrix<4, 4> translate3D(double dx, double dy, double dz) {
  return {1.0, 0.0, 0.0, 0.0,
          0.0, 1.0, 0.0, 0.0,
          0.0, 0.0, 1.0, 0.0,
           dx,  dy,  dz, 1.0};
}

/**
 * \brief Constructs a 3D scaling matrix.
 * 
 * \param[in] sx, sy, sz Scales of each coordinate.
 * \return Scaling matrix.
 */
constexpr Matrix<4, 4> scale3D(double sx, double sy, double sz) {
  return { sx, 0.0, 0.0, 0.0,
          0.0,  sy, 0.0, 0.0,
          0.0, 0.0,  sz, 0.0,
          0.0, 0.0, 0.0, 1.0};
}

/**
 * \brief Constructs a view matrix.
 * 
 * Returns a view matrix for a camera positioned at \p eye, looking at
 * \p center, with the \p view_up vector orienting the camera.
 * 
 * \param[in] eye     Position of eye.
 * \param[in] center  Center of view.
 * \param[in] view_up Camera up vector.
 * \tparam V1, V2, V3 Types of vectors.
 * \return View matrix.
 */
template <class V1, class V2, class V3>
constexpr Matrix<4, 4> look_at(V1 eye, V2 center, V3 view_up) {
  static_assert(eye.dimension() == 3 && center.dimension() == 3 && view_up.dimension() == 3,
    "Invalid parameter dimensions to look_at");

  Vector<3> fwd = (eye - center).normalize();
  Vector<3> side = view_up.x(fwd).normalize();
  Vector<3> up = fwd.x(side);

  return { side[0],        up[0],         fwd[0], 0.0,
           side[1],        up[1],         fwd[1], 0.0,
           side[2],        up[2],         fwd[2], 0.0,
    - (side * eye), - (up * eye),  - (fwd * eye), 1.0};
}

/**
 * \brief Constructs a projection matrix.
 * 
 * \param[in] l, r Coordinates for the left and right clipping planes.
 * \param[in] b, t Coordinates for the bottom and top clipping planes.
 * \param[in] n, f Distances to the near and far clipping planes.
 * \return Projection matrix.
 */
constexpr Matrix<4, 4> frustum(double l, double r, double b, double t, int n, int f) {
  return { 2 * n / (r - l),               0.0,                          0.0,  0.0,
                       0.0,   2 * n / (t - b),                          0.0,  0.0,
         (r + l) / (r - l), (t + b) / (t - b), - (f + n) / (double) (f - n), -1.0,
                       0.0,               0.0,       -2.0 * f * n / (f - n),  0.0};
}

}  // namespace math

#endif
