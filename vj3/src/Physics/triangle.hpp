#pragma once

#include <glm/vec3.hpp>

namespace Physics {
  struct triangle {
  glm::vec3 p1, p2, p3;

  };
}