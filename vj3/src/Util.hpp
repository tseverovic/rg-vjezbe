/**
 * @file Util.hpp
 * @author Tomislav Severović
 * @brief Odds and ends.
 * @version 0.1
 * @date 2019-01-15
 * 
 * @copyright Copyright (c) 2019 Tomislav Severović
 * 
 */
#pragma once

#include <type_traits>

namespace Util {

/**
 * @brief Casts an enum to its underlying type.
 * 
 * @tparam Enum Type of enum.
 * @param e The enum to cast.
 * @return Underlying value of e.
 */
template <typename Enum>
constexpr auto enum_cast(Enum e) {
  return static_cast<std::underlying_type_t<Enum>>(e);
}

}  // namespace Util