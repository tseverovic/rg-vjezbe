#include "Meshes.hpp"

#include <cassert>
#include <cmath>
#include <array>
#include <algorithm>

namespace Meshes {

auto box_mesh() -> mesh_arrays {
  // normals for Top, Bottom, Front, bacK, Left and Right faces
  constexpr glm::vec3 t_normal{0.0f, +1.0f, 0.0f};
  constexpr glm::vec3 b_normal{0.0f, -1.0f, 0.0f};
  constexpr glm::vec3 f_normal{0.0f, 0.0f, +1.0f};
  constexpr glm::vec3 k_normal{0.0f, 0.0f, -1.0f};
  constexpr glm::vec3 l_normal{-1.0f, 0.0f, 0.0f};
  constexpr glm::vec3 r_normal{+1.0f, 0.0f, 0.0f};

  std::vector<vert_data> verts{
      {{-1.0f, +1.0f, -1.0f}, t_normal},  // 0
      {{+1.0f, +1.0f, -1.0f}, t_normal},  // 1
      {{+1.0f, +1.0f, +1.0f}, t_normal},  // 2
      {{-1.0f, +1.0f, +1.0f}, t_normal},  // 3

      {{-1.0f, +1.0f, -1.0f}, k_normal},  // 4
      {{+1.0f, +1.0f, -1.0f}, k_normal},  // 5
      {{+1.0f, -1.0f, -1.0f}, k_normal},  // 6
      {{-1.0f, -1.0f, -1.0f}, k_normal},  // 7

      {{-1.0f, +1.0f, +1.0f}, l_normal},  // 8
      {{-1.0f, +1.0f, -1.0f}, l_normal},  // 9
      {{-1.0f, -1.0f, +1.0f}, l_normal},  // 10
      {{-1.0f, -1.0f, -1.0f}, l_normal},  // 11

      {{-1.0f, +1.0f, +1.0f}, f_normal},  // 12
      {{-1.0f, -1.0f, +1.0f}, f_normal},  // 13
      {{+1.0f, +1.0f, +1.0f}, f_normal},  // 14
      {{+1.0f, -1.0f, +1.0f}, f_normal},  // 15

      {{+1.0f, +1.0f, -1.0f}, r_normal},  // 16
      {{+1.0f, +1.0f, +1.0f}, r_normal},  // 17
      {{+1.0f, -1.0f, +1.0f}, r_normal},  // 18
      {{+1.0f, -1.0f, -1.0f}, r_normal},  // 19

      {{-1.0f, -1.0f, -1.0f}, b_normal},  // 20
      {{+1.0f, -1.0f, -1.0f}, b_normal},  // 21
      {{+1.0f, -1.0f, +1.0f}, b_normal},  // 22
      {{-1.0f, -1.0f, +1.0f}, b_normal},  // 23
  };

  // CCW front
  std::vector<GLuint> indices{
      // bottom face
      0, 2, 1,  // 0
      0, 3, 2,  // 1

      // front face
      4, 5, 6,  // 2
      4, 6, 7,  // 3

      // left face
      8, 9, 10,   // 4
      10, 9, 11,  // 5

      // back face
      12, 13, 14,  // 6
      13, 15, 14,  // 7

      // right face
      16, 17, 18,  // 8
      18, 19, 16,  // 9

      // top face
      20, 21, 23,  // 10
      23, 21, 22   // 11
  };

  return mesh_arrays{std::move(verts), std::move(indices)};
}

auto make_sphere_indices(unsigned lat_zones, unsigned long_zones) {
  // 2 tris for each face in zone
  // 1 tri for each latitude zone on each pole
  std::vector<GLuint> sphere_indices(3 * 2 * (lat_zones - 1) * long_zones);

  // FRONT = CCW
  auto index_it = sphere_indices.begin();
  std::array<GLuint, 3> tri_indices;
  auto tri_it = tri_indices.begin();
  for (unsigned i = 0; i < long_zones - 1; ++i) {
    tri_indices = {i + 2, i + 1, 0};
    index_it = std::copy_n(tri_it, 3, index_it);
  }
  tri_indices = {1, long_zones, 0};
  index_it = std::copy_n(tri_it, 3, index_it);

  for (unsigned j = 1; j < lat_zones - 1; ++j) {
    // + 1 to account for pole vertex
    auto lat_index = j * long_zones + 1;
    auto above_lat_index = (j - 1) * long_zones + 1;

    for (unsigned i = 0; i < long_zones - 1; ++i) {
      tri_indices = {1 + i + above_lat_index, i + lat_index,
                     i + above_lat_index};
      index_it = std::copy_n(tri_it, 3, index_it);
      tri_indices = {1 + i + lat_index, i + lat_index, 1 + i + above_lat_index};
      index_it = std::copy_n(tri_it, 3, index_it);
    }
    tri_indices = {above_lat_index, long_zones + lat_index - 1,
                   long_zones + above_lat_index - 1};
    index_it = std::copy_n(tri_it, 3, index_it);
    tri_indices = {lat_index, long_zones + lat_index - 1, above_lat_index};
    index_it = std::copy_n(tri_it, 3, index_it);
  }

  auto lat_index = (lat_zones - 2) * long_zones + 1;
  for (unsigned i = 0; i < long_zones - 1; ++i) {
    tri_indices = {i + lat_index, 1 + i + lat_index, (lat_zones - 1) * long_zones + 1};
    index_it = std::copy_n(tri_it, 3, index_it);
  }
  tri_indices = {long_zones + lat_index - 1, lat_index, (lat_zones - 1) * long_zones + 1};
  index_it = std::copy_n(tri_it, 3, index_it);

  assert(index_it == sphere_indices.end());

  return sphere_indices;
}

auto sphere_mesh(unsigned lat_zones, unsigned long_zones) -> mesh_arrays {
  std::vector<glm::vec3> ring(long_zones);
  for (int i = 0; i < long_zones; ++i) {
    float t = (float)i / long_zones * M_PI * 2;
    ring[i] = glm::vec3{std::cos(t), 0.0f, std::sin(t)};
  }

  // normals are equal to verts. simple!

  // capped off with single point on both ends
  std::vector<vert_data> verts((lat_zones - 1) * long_zones + 2);

  auto sphere_it = verts.begin();
  *sphere_it = {{0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f}};

  ++sphere_it;
  for (unsigned i = 1; i < lat_zones; ++i) {
    float t = (float)i / lat_zones * M_PI;
    float y = std::cos(t);
    float xz_scale = std::sin(t);
    sphere_it = std::transform(ring.cbegin(), ring.cend(), sphere_it,
                               [xz_scale, y](glm::vec3 point) {
                                 point.x *= xz_scale;
                                 point.y = y;
                                 point.z *= xz_scale;
                                 return vert_data{point, point};
                               });
  }
  *sphere_it = {{0.0f, -1.0f, 0.0f}, {0.0f, -1.0f, 0.0f}};
  assert(++sphere_it == verts.end());

  return mesh_arrays{std::move(verts),
                     make_sphere_indices(lat_zones, long_zones)};
}
auto send_mesh(std::vector<GLuint>& buffers, std::vector<GLuint>& vaos,
               mesh_arrays const& arrays) -> object_draw_data {
  auto&& [verts, indices] = arrays;
  size_t verts_size = sizeof(verts[0]) * verts.size();
  size_t indices_size = sizeof(indices[0]) * indices.size();

  vaos.resize(vaos.size() + 1);
  buffers.resize(buffers.size() + 2);

  glCreateVertexArrays(1, vaos.data() + vaos.size() - 1);
  glCreateBuffers(2, buffers.data() + buffers.size() - 2);

  GLuint vert_data_buf = buffers[buffers.size() - 2];
  GLuint index_buf = buffers.back();
  GLuint vao = vaos.back();
  glNamedBufferStorage(vert_data_buf, verts_size, verts.data(), 0);
  glNamedBufferStorage(index_buf, indices_size, indices.data(), 0);

  glVertexArrayVertexBuffer(vao, 0, vert_data_buf, 0, sizeof(vert_data));

  glEnableVertexArrayAttrib(vao, 0);
  glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE,
                            offsetof(vert_data, pos));
  glVertexArrayAttribBinding(vao, 0, 0);

  glEnableVertexArrayAttrib(vao, 1);
  glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, GL_FALSE,
                            offsetof(vert_data, normal));
  glVertexArrayAttribBinding(vao, 1, 0);

  glVertexArrayElementBuffer(vao, index_buf);

  // TODO: use indirect rendering
  return object_draw_data{vao, (GLuint)indices.size()};
}

}  // namespace Meshes