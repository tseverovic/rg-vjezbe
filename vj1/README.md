# Vježba 1 - B-krivulja

![Avion leti, prateći B-krivulju](./doc/avion.png)

Program učitava model iz OBJ datoteke i kontrolne točke kubne B-krivulje, te
iscrtava scenu gdje se učitani model pokreće po B-krivulji. Moguće je pomicati
kameru po sceni, ili zadati da prati model dok se kreće. Može se i promijeniti
kako se model orijentira po B-krivulji.

## Build

Potreban je Cmake verzije najmanje 3.11 te C++ prevodioc koji podržava standard
C++17. Također su potrebne biblioteke GL, glut i GLU.

Navedeni koraci su za Linux, na Windowsima je postupak vjerojatno malo drugačiji.

1. Otvorite novi direktorij (npr `build`)
2. U direktoriju `build`  pokrenite naredbu `cmake ..`
3. Trebale bi se generirati build datoteke za neki alat (tipično `make`).
4. Pokrenite naredbu `make`.
5. Trebala bi se izgraditi izvršna datoteka `putanja`.

## Pokretanje

Program traži dva argumenta komandne linije, putanju do OBJ datoteke i putanju
do datoteke koja sadrži kontrolne točke B-krivulje.

Program iscrtava B-krivulju crvenom bojom, tangente na B-krivulji zelenom bojom,
i objekt učitan iz datoteke kako se pomiče po krivulji.

Moguće je kontrolirati kameru tipkovnicom: strelicama se pomiče u x-z ravnini
a `<SPC>` i `c` pomiču kameru u y smjeru. Pritiskom na `y` kamera prati objekt.
`-` smanjuje vidno polje, a `+` povećava (zoom in/out).

Pritiskom na `x` mijenja se izračun orijentacije objekta. Moguć je izbor između
izračuna DCM matrice objekta preko tangente i normale B-krivulje, te rotacije
objekta po jednoj osi tako da se njegova z-os poklopi s tangentom krivulje.

Pritiskom na `f` mijenja se stanje odbacivanja stražnje strane objekta.

## Komentari

Za crtanje se koriste immediate-mode OpenGL naredbe, što nije naročito efikasno.

Kod izračuna transformacije objekta s DCM matricom se dobivena matrica invertira,
što nije potrebno jer je rotacijska matrica ortogonalna, i bolje bi bilo da se
transponira.

Kada kamera prati objekt mijenjanje pozicije kamere ju samo pomiče po osima
umjesto da ju rotira oko objekta, što nije očekivano ponašanje. Ovo se može
riješiti tako da se za prateću kameru mijenjaju sferne koordinate kamere.
