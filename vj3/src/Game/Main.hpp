#pragma once

#include <SDL2/SDL_video.h>

namespace Game {

/**
 * @brief Starts the game. Initializes the structures and starts loop.
 * 
 * @param window Window to draw to.
 */
auto main(SDL_Window* window) noexcept -> void;

}