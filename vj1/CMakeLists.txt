cmake_minimum_required(VERSION 3.11)

set(CMAKE_CXX_STANDARD 17)

add_executable(putanja src/main.cpp)
target_sources(putanja PUBLIC src/ObjectModel.cpp src/bspline.cpp)
target_include_directories(putanja PUBLIC lib)
target_link_libraries(putanja PUBLIC GL glut GLU)