#pragma once

#include "box.hpp"
#include "convex_hull.hpp"
#include "cylinder.hpp"
#include "triangle.hpp"
#include "sphere.hpp"
#include "Collision.hpp"

#include <variant>

namespace Physics {
/*
using element = std::variant<box, convex_hull, cylinder, element_union, sphere, pill>;
*/
using element = std::variant<sphere, box>;

}  // namespace Physics
