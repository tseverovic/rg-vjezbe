/**
 * @file Vector.hpp
 * @brief Header file of the Vector class
 *
 * @author Tomislav Severović
 */

#ifndef math_Vector_HPP
#define math_Vector_HPP

#include <algorithm>
#include <array>
#include <iosfwd>
#include <type_traits>
#include <utility>

#include <cmath>

/**
 * @brief Contains mathematical utilities.
 */
namespace math {

namespace detail {

// forward declaration
template <std::size_t N, class Vec>
class MutableVectorBase;

template <std::size_t N, class Vec>
class VectorBase;

}  // namespace detail

/**
 * @brief Simple concrete vector class.
 *
 * The Vector class represents a N-dimensional mathematical vector.
 * It supports basic vector calculus operations with vectors of the same
 * dimensionality and can be converted to a row or column matrix.
 *
 * @author Tomislav Severović
 */
template <std::size_t N
          ///< Number of dimensions of vector.
          >
class Vector : public detail::MutableVectorBase<N, Vector<N>> {
  using storage_type = std::array<double, N>;
  /**
   * @brief std::array containing elements of vector.
   */
  storage_type _elements = {};

 public:
  // TODO: pass by value for small vectors

  /**
   * @brief Brace initialization constructor.
   *
   * @param[in] args... Elements of vector.
   * @tparam T... Types of arguments.
   */
  template <class... T>
  constexpr Vector(T... args) : _elements{{args...}} {}

  constexpr Vector() = default;
  constexpr Vector(const Vector&) = default;

  constexpr Vector& operator=(const Vector&) = default;

  template <std::size_t N1, template <std::size_t> class VecArg>
  constexpr Vector(const VecArg<N1>& vec) {
    std::copy_n(vec.elements().begin(), std::min(N, N1), _elements.begin());
  }

  constexpr const storage_type& elements() const { return _elements; }
  constexpr storage_type& elements() { return _elements; }

  /**
   * @brief Indexing operator.
   *
   * @param[in] i Index of element in vector.
   * @return Reference to element of vector.
   *
   * @warning No bounds checking is performed.
   */
  constexpr double& operator[](std::size_t i) { return _elements[i]; }

  /**
   * @brief Indexing operator.
   *
   * @param[in] i Index of element in vector.
   * @return Reference to element of vector.
   *
   * @warning No bounds checking is performed.
   */
  constexpr const double& operator[](std::size_t i) const {
    return _elements[i];
  }
};

namespace detail {

/**
 * @brief Base class for all Vectors.
 *
 * Defines most operations on Vectors.
 *
 * @author Tomislav Severović
 */
template <std::size_t N,  ///< Number of dimensions of vector.
          class Vec       ///< Subclass.
          >
class VectorBase {
  // convenience type aliases
  using CVec = Vector<N>;  // concrete vector with same dimensions as this
  static constexpr std::size_t dimen_ = N;

 protected:
  constexpr VectorBase() = default;

  constexpr const Vec& _refine() const {
    return static_cast<const Vec&>(*this);
  }

 public:
  constexpr std::size_t dimension() const { return dimen_; }

// TODO: enable for both static sized and dynamic sized vectors
#define size_check(arg) static_assert(N == arg.dimension(), "Vector dimensions don't match")
  /**
   * @brief Vector addition.
   *
   * @param[in] v Second term.
   * @tparam VecArg Type of second term.
   * @return Sum of two vectors.
   */
  template <class VecArg>
  constexpr CVec operator+(const VecArg& v) const {
    size_check(v);
    return CVec(_refine()) += v;
  }

  /**
   * @brief Vector subtraction.
   *
   * @param[in] v Subtrahend.
   * @tparam VecArg Type of subtrahend.
   * @return Difference of the two vectors.
   */
  template <class VecArg>
  constexpr CVec operator-(const VecArg& v) const {
    size_check(v);
    return CVec(_refine()) -= v;
  }

  /**
   * @brief Vector negation.
   *
   * @return Negated vector.
   */
  constexpr CVec operator-() const { return CVec(_refine()).negate(); }

  /**
   * @brief Scalar multiplication.
   *
   * @param[in] x Factor.
   * @return Scaled vector.
   */
  constexpr CVec operator*(double x) const { return CVec(_refine()) *= x; }

  /**
   * @brief Scalar multiplication.
   *
   * @param[in] x Factor.
   * @param[in] vec Vector being scaled.
   * @return Scaled vector.
   */
  constexpr friend CVec operator*(double x, const Vec& vec) {
    return vec * x;
  }

  /**
   * @brief Scalar product of two vectors.
   *
   * @param[in] v Other vector.
   * @tparam VecArg Type of argument.
   * @return Scalar product of two vectors.
   */
  template <class VecArg>
  constexpr double operator*(const VecArg& v) const {
    size_check(v);
    double sum = 0.0;
    for (std::size_t i = 0; i < N; i++) {
      sum += _refine()[i] * v[i];
    }
    return sum;
  }

  /**
   * @brief Returns the vector's Euclidean norm.
   *
   * @return Norm of vector.
   */
  constexpr double norm() const { return std::sqrt(_refine() * _refine()); }

  /**
   * @brief Returns the normalized vector.
   *
   * @return Normalized vector.
   */
  constexpr CVec normalized() const { return CVec(_refine()).normalize(); }

  /**
   * @brief Returns the cosine of the angle between two vectors.
   *
   * @param[in] v Other vector.
   * @tparam VecArg Type of argument.
   * @return Cosine of angle between this and other vector.
   */
  template <class VecArg>
  constexpr double cosine(const VecArg& v) const {
    size_check(v);

    double this_norm2 = _refine() * _refine();
    double v_norm2 = v * v;

    return (_refine() * v) / std::sqrt(this_norm2 * v_norm2);
  }

  /**
   * @brief Vector cross product.
   *
   * @param v[in] Other vector.
   * @tparam VecArg Type of argument.
   * @return Cross product of two vectors.
   */
  template <class VecArg, std::size_t N1 = N>
  constexpr std::enable_if_t<N1 == 3, Vector<3>> x(VecArg v) const {
    // conditinally compiled
    const Vec& u = _refine();

    return {u[1] * v[2] - u[2] * v[1],
          u[2] * v[0] - u[0] * v[2],
          u[0] * v[1] - u[1] * v[0]};
  }

  /**
   * /brief Elementwise multiplication of vectors.
   * 
   * /param[in] v Other vector.
   * /tparam VecArg Type of argument
   * /return Elementwise multiplication of two vectors.
   */
  template <class VecArg>
  constexpr CVec element_mul(VecArg v) const {
    size_check(v);

    CVec result(_refine());
    for (std::size_t i = 0; i < N; i++) {
      result[i] *= v[i];
    }

    return result;
  }

  /**
   * /brief Vector reflection.
   * 
   * /param[in] v Vector to reflect off of.
   * /tparam VecArg Type of argument
   * /return This vector, reflected off of <tt>v</tt>.
   */
  template <class VecArg>
  constexpr CVec reflected(VecArg v) const {
    size_check(v);

    CVec n = v.normalized();

    return _refine() - 2 * (_refine() * n) * n;
  }

  /**
   * @brief Converts vector from homogenous form.
   *
   * @return Non-homogenous vector.
   */
  template <std::size_t N1 = N>
  constexpr std::enable_if_t<(N1 > 1), Vector<N - 1>> from_homogenous()
      const {
    // conditionally compiled
    Vector<N - 1> result;
    const double hmg_factor = _refine()[N - 1];

    for (std::size_t i = 0; i < N - 1; i++) {
      result[i] = _refine()[i] / hmg_factor;
    }

    return result;
  }

  /**
   * @brief Writes a vector to an std::ostream.
   *
   * @param[out] os std::ostream to write to.
   * @param[in] v Vector to write.
   * @return os.
   */
  friend std::ostream& operator<<(std::ostream& os, const Vec& v) {
    if (N == 0) return os;

    for (std::size_t i = 0; i < N - 1; i++) {
      os << v[i] << ' ';
    }
    return os << v[N - 1];
  }

  template <class VecArg>
  constexpr bool operator==(const VecArg& v) const {
    if (N != v.dimension()) return false;

    for (std::size_t i = 0; i < N; i++) {
      if (_refine()[i] != v[i]) return false;
    }

    return true;
  }
};  // class VectorBase


/**
 * @brief Base class for mutable vectors.
 * 
 * Defines operations which mutate the current vector.
 */
template <std::size_t N,
          class Vec
          >
class MutableVectorBase : public VectorBase<N, Vec> {
  // convenience type aliases
  using CVec = Vector<N>;  // concrete vector with same dimensions as this

  /**
   * @brief Get *this as reference to subclass.
   *
   * @return Equivalent to static_cast<Vec&>(*this).
   */

 protected:
  constexpr Vec& _refine() { return static_cast<Vec&>(*this); }

  constexpr MutableVectorBase() = default;

 public:
  /**
   * @brief In place vector addition.
   * @note The vector is modified by this operation.
   *
   * @param[in] v Second term.
   * @tparam VecArg Type of second term.
   * @return Reference to *this.
   */
  template <class VecArg>
  constexpr Vec& operator+=(const VecArg& v) {
    size_check(v);
    for (std::size_t i = 0; i < N; i++) {
      _refine()[i] += v[i];
    }

    return _refine();
  }

  /**
   * @brief In place vector subtraction.
   * @note The vector is modified by this operation.
   *
   * @param[in] v Subtrahend.
   * @tparam VecArg Type of subtrahend.
   * @return Reference to *this.
   */
  template <class VecArg>
  constexpr Vec& operator-=(const VecArg& v) {
    size_check(v);
    for (std::size_t i = 0; i < N; i++) {
      _refine()[i] -= v[i];
    }

    return _refine();
  }

  /**
   * @brief In place vector negation.
   * @note The vector is modified by this operation.
   *
   * @return Reference to *this.
   */
  constexpr Vec& negate() {
    for (std::size_t i = 0; i < N; i++) {
      _refine()[i] = -_refine()[i];
    }

    return _refine();
  }

  /**
   * @brief In place scalar multiplication.
   * @note The vector is modified by this operation.
   *
   * @param[in] x Factor.
   * @return Reference to *this.
   */
  constexpr Vec& operator*=(double x) {
    for (std::size_t i = 0; i < N; i++) {
      _refine()[i] *= x;
    }

    return _refine();
  }

  /**
   * @brief In place scalar division.
   * @note The vector is modified by this operation.
   *
   * @param[in] x Divisor.
   * @return Reference to *this.
   */
  constexpr Vec& operator/=(double x) {
    for (std::size_t i = 0; i < N; i++) {
      _refine()[i] /= x;
    }

    return _refine();
  }

  /**
   * @brief Normalizes the vector.
   * @note The vector is modified by this operation.
   *
   * @return Reference to *this.
   */
  constexpr Vec& normalize() {
    double norm_v = _refine().norm();
    for (std::size_t i = 0; i < N; i++) {
      _refine()[i] /= norm_v;
    }

    return _refine();
  }

  /**
   * @brief Reads a vector from an std::istream.
   *
   * @param[in] is std::istream to read from.
   * @param[out] v Vector to update.
   * @return is.
   */
  friend std::istream& operator>>(std::istream& is, Vec& v) {
    for (std::size_t i = 0; i < N; i++) {
      is >> v[i];
    }
    return is;
  }

}; // class MutableVectorBase
#undef size_check
}  // namespace detail

}  // namespace math

#endif
