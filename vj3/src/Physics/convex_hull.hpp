#pragma once

#include <vector>
#include <glm/vec3.hpp>

namespace Physics {

struct convex_hull {
  std::vector<glm::vec3> elems;
};

}