#pragma once

#include <glm/vec3.hpp>

namespace Physics {

struct box {
  float halfwidth;
  float halfheight;
  float halfdepth;
};

}