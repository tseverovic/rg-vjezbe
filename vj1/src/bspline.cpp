#include "bspline.hpp"

#include <cmath>

#include "math/Matrix.hpp"
#include "math/Vector.hpp"

auto ri(const std::vector<math::Vector<3>>& r, int t) -> math::Matrix<4, 3> {
  return {r[t - 1][0], r[t - 1][1], r[t - 1][2],
          r[t][0],     r[t][1],     r[t][2],
          r[t + 1][0], r[t + 1][1], r[t + 1][2],
          r[t + 2][0], r[t + 2][1], r[t + 2][2]};
}

auto bspline_vector(const std::vector<math::Vector<3>>& r, double t)
    -> std::optional<math::Vector<3>> {
  if (t < 0 || t > r.size() - 3) return {};

  double i_f;
  t = std::modf(t, &i_f);
  int i = static_cast<int>(i_f) + 1;

  math::Vector<4> ts = {t * t * t, t * t, t, 1.0};

  constexpr static math::Matrix<4, 4> m{-1.0,  3.0, -3.0, 1.0,
                                         3.0, -6.0,  3.0, 0.0,
                                        -3.0,  0.0,  3.0, 0.0,
                                         1.0,  4.0,  1.0, 0.0};

  return {math::to_vector(math::row_matrix(ts * (1.0 / 6.0)) * m * ri(r, i))};
}

auto bspline_tangent(const std::vector<math::Vector<3>>& r, double t)
    -> std::optional<math::Vector<3>> {
  if (t < 0 || t > r.size() - 3) return {};

  double i_f;
  t = std::modf(t, &i_f);
  int i = static_cast<int>(i_f) + 1;

  math::Vector<3> ts = {t * t, t, 1.0};

  constexpr static math::Matrix<3, 4> m{-1.0,  3.0, -3.0, 1.0,
                                         2.0, -4.0,  2.0, 0.0,
                                        -1.0,  0.0,  1.0, 0.0};

  return {math::to_vector(math::row_matrix(ts * 0.5) * m * ri(r, i))};
}

auto bspline_2deriv(const std::vector<math::Vector<3>>& r, double t)
    -> std::optional<math::Vector<3>> {
  if (t < 0 || t > r.size() - 3) return {};

  double i_f;
  t = std::modf(t, &i_f);
  int i = static_cast<int>(i_f) + 1;

  math::Vector<2> ts = {t, 1.0};

  constexpr static math::Matrix<2, 4> m{-1.0,  3.0, -3.0, 1.0,
                                         1.0, -2.0,  1.0, 0.0};

  return {math::to_vector(math::row_matrix(ts) * m * ri(r, i))};
}