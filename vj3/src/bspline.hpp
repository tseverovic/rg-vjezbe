#pragma once

#include <vector>
#include <optional>
#include <utility>

#include "math/Matrix.hpp"
#include "math/Vector.hpp"

/**
 * Point on uniform cubic B-spline.
 * 
 * /param r std::vector of points defining spline path
 * /param t spline parameter
 * /return std::nullopt if t is out of range, point on B-spline otherwise
 */
auto bspline_vector(const std::vector<math::Vector<3>>& r, double t) -> std::optional<math::Vector<3>>;


/**
 * Tangent (first derivation) of cubic B-spline at a point.
 * 
 * /param r std::vector of points defining spline path
 * /param t spline parameter
 * /return std::nullopt if t is out of range, tangent of point on B-spline otherwise
 */
auto bspline_tangent(const std::vector<math::Vector<3>>& r, double t) -> std::optional<math::Vector<3>>;

/**
 * Second derivation of uniform cubic B-spline at a point.
 * 
 * /param r std::vector of points defining spline path
 * /param t spline parameter
 * /return std::nullopt if t is out of range, second derivation of B-spline otherwise
 */
auto bspline_2deriv(const std::vector<math::Vector<3>>& r, double t) -> std::optional<math::Vector<3>>;