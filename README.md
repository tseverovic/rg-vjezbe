# Računalna grafika - Laboratorijske vježbe

Rješenja laboratorijskih vježbi iz predmeta Računalna grafika, ak. god.
2018./2019.

## Zadaci

### [Vježba 1 - B-krivulja](./vj1)

![Avion leti, prateći B-krivulju](./vj1/doc/avion.png)

### [Vježba 2 - Sustav čestica](./vj2)

![Tisuće pahulja na tamnoplavoj pozadini](./vj2/doc/pahulje.png)

### [Vježba 3 - Detekcija kolizije](./vj3)
