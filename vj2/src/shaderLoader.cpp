#include "shaderLoader.hpp"

#include <iostream>
#include <iterator>
#include <fstream>


auto shaderFileLoader(std::string_view sh, GLenum type) -> std::optional<GLuint> {
  std::string code;
  if (std::ifstream file{sh.data(), std::ios::in | std::ios::ate}) {
    auto size = file.tellg();
    file.seekg(0);
    code.append(std::istreambuf_iterator<char>{file}, std::istreambuf_iterator<char>{});
    if (file.fail()) {
      std::cerr << "Failed reading file " << sh << '\n';
      return {};
    }
  } else {
    std::cerr << "Failed to open file " << sh << '\n';
    return {}; 
  }

  GLuint shader_id = glCreateShader(type);
  const auto* c_code = code.c_str();
  glShaderSource(shader_id, 1, &c_code, NULL);
  glCompileShader(shader_id);

  GLint result;
  GLint log_length;
  glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
  glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);
  if (log_length > 0) {
    std::string log(log_length, 'x');
    glGetShaderInfoLog(shader_id, log_length, NULL, log.data());
    std::clog << "Compile log for shader " << sh << '\n' << log;
  }

  if (result == GL_FALSE) {
    // TODO: use RAII deletion instead of this.
    glDeleteShader(shader_id);
    return {};
  } else {
    return shader_id;
  }
}

auto loadShader(std::string_view vsh, std::string_view fsh) -> std::optional<GLuint> {
  GLuint vsh_id;
  if (auto vsh_o = shaderFileLoader(vsh, GL_VERTEX_SHADER)) {
    vsh_id = *vsh_o;
  } else {
    return {};
  }
  GLuint fsh_id;
  if (auto fsh_o = shaderFileLoader(fsh, GL_FRAGMENT_SHADER)) {
    fsh_id = *fsh_o;
  } else {
    glDeleteShader(vsh_id);
    return {};
  }

  GLuint program = glCreateProgram();
  glAttachShader(program, vsh_id);
  glAttachShader(program, fsh_id);
  glLinkProgram(program);

  GLint result;
  GLint log_length;
  glGetProgramiv(program, GL_LINK_STATUS, &result);
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
  if (log_length > 0) {
    std::string log(log_length, 'x');
    glGetShaderInfoLog(program, log_length, NULL, log.data());
    std::clog << "Link log for program " << vsh << ", " << fsh << '\n' << log << '\n';
  }

  glDetachShader(program, vsh_id);
  glDetachShader(program, fsh_id);
  glDeleteShader(vsh_id);
  glDeleteShader(fsh_id);

  if (result == GL_FALSE) {
    glDeleteProgram(program);
    return {};
  } else {
    return program;
  }
}
