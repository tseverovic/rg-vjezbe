#pragma once

#include <vector>
#include <iosfwd>
#include <GL/freeglut.h>

#include <variant>

#include "math/Vector.hpp"
#include "math/Matrix.hpp"

struct TriIndices {
  GLuint is[3];
};

struct PackedVert {
  math::Vector<3> vert;
  math::Vector<3> normal;
};

struct Face3D {
  double a, b, c, d;
};

class ObjectModel {
  struct GLRotate {
    double angle, x, y, z;
  };
  using DCMRotate = math::Matrix<4, 4>;

  std::vector<TriIndices> indices_;
  std::vector<PackedVert> verts_;
  std::vector<Face3D> faces_;

  std::variant<GLRotate, DCMRotate> rot_;

  math::Vector<3> pos_;

  void compute_normals();
public:
  ObjectModel() = default;
  ObjectModel(const ObjectModel&) = default;
  ObjectModel& operator=(const ObjectModel&) = default;
  ObjectModel(ObjectModel&&) = default;
  ObjectModel& operator=(ObjectModel&&) = default;

  void pos(math::Vector<3> pos);
  auto pos() const -> const math::Vector<3>&;
  void set_rotate_dcm(const math::Matrix<4, 4>& dcm);
  void set_rotate_gl(double angle, const math::Vector<3>& axis);

  void draw() const;

  friend auto operator>>(std::istream& is, ObjectModel& om) -> std::istream&;
};