#include "Log.hpp"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <chrono>
#include <cstdlib>
#include <sstream>

namespace Log {

namespace {

/// File stream, if open and used.
std::ofstream file_stream;
/// Name of program.
std::string_view program_name;
/// Log stream sync.
std::mutex log_mutex;

/**
 * @brief Prints ISO date and time to os.
 * 
 * @param os Stream to print to.
 * @return os
 */
auto print_time(std::ostream& os) noexcept -> std::ostream& {
  using namespace std::chrono;
  auto now_c = system_clock::to_time_t(system_clock::now());
  return os << std::put_time(std::localtime(&now_c), "%F %T");
}

}  // namespace

logstream::logstream() noexcept : buf_{} {
  base_stream::init(&buf_);
  *this << print_time << ' ';
}

logstream::logstream(logstream&& src) noexcept
    : base_stream{std::move(src)}, buf_{std::move(src.buf_)} {
  base_stream::set_rdbuf(&buf_);
}

logstream::~logstream() noexcept {
  std::lock_guard log_guard{log_mutex};
  std::clog << &buf_ << '\n';
}

auto init(std::string_view program_name, std::string_view filename) noexcept
    -> void {
  std::ios::sync_with_stdio(false);

  Log::program_name = program_name;
  if (filename == "stdout") {
    std::clog.rdbuf(std::cout.rdbuf());
  } else if (filename != "stderr") {
    file_stream = std::ofstream{filename.data(), std::ios::ate};
    if (!file_stream) {
      std::cerr << "Failed to open file \"" << filename << "\"!\n";
      std::exit(EXIT_FAILURE);
    }
    std::clog.rdbuf(file_stream.rdbuf());
  }

  std::clog << program_name << " log start " << print_time << '\n';
}

}  // namespace Log